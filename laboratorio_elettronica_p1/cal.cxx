#include "TF1.h"
#include "TGraphErrors.h"
#include "TAxis.h"
#include "TCanvas.h"
#include "TStyle.h"
#include "uncertainty_generation/egen.hxx"
#include "Math/Minimizer.h"
#include "TFitResult.h"
#include "TFitResultPtr.h"
#include <fstream>
#include <iostream>
int main()
{
    gStyle->SetOptTitle(1);
    gStyle->SetOptStat(10);
    gStyle->SetOptFit(1110);
    gStyle->SetStatY(0.9);
    gStyle->SetStatX(0.425);
    gStyle->SetStatW(0.18);
    gStyle->SetStatH(0.18);
    ROOT::Math::MinimizerOptions::SetDefaultErrorDef(4.);
    Double_t w = 3000;
    Double_t h = 2000;
    TCanvas *canvas = new TCanvas();
    canvas->SetTitle("Calibrazione");
    canvas->SetWindowSize(w, h);
    canvas->SetGridx();
    canvas->SetGridy();

    std::ifstream data;
    data.open("data_cal.dat");
    int i = 0;
    double v;
    while (data >> v)
    {
        ++i;
    }
    std::cout << i << '\n';
    data.close();
    if ((i % 4) != 0)
    {
        throw std::logic_error("Wrong Data Format");
    }
    data.open("data_cal.dat");
    i = i / 4;
    double v_o[i];
    double ev_o[i];
    double v_m[i];
    double ev_m[i];
    for (int ii = 0; ii != i; ++ii)
    {
        data >> v_o[ii] >> ev_o[ii] >> v_m[ii] >> ev_m[ii];
        ev_o[ii] = feV(v_o[ii],ev_o[ii]);
        ev_m[ii] = femV(v_m[ii],ev_m[ii]);
    }
    std::cout << v_o[0] << '\n';
    TGraphErrors cal(i, v_m, v_o, ev_m, ev_o);
    TF1 line("Cal", "pol1");

    line.SetNpx(1e3);
    line.SetLineColorAlpha(kBlue,0.4);
    line.SetLineWidth(2);
    cal.SetMarkerStyle(8);
    cal.SetMarkerSize(1.2);

    line.SetParName(0, "Intercept");
    line.SetParName(1, "Angular Coefficient");
    cal.SetTitle("Calibrazione");

    cal.GetYaxis()->SetTitle("#mathrm{V_{Oscilloscope} (V)}");
    cal.GetXaxis()->SetTitle("#mathrm{V_{Multimeter} (V)}");
    auto p = cal.Fit(&line,"S,E");
    auto pr = p.Get();
    auto cm = pr->GetCovarianceMatrix();
    cm.Print();
    std::cout << "I0 error: " << pr->LowerError(0) << "\t" << pr->UpperError(0) << '\n';
    std::cout << "etaVt error: " << pr->LowerError(1) << "\t" << pr->UpperError(1) << '\n'; 
    cal.Draw("AP");
    canvas->Print("Cal.png");
    canvas->Print("Calt.tex");
    std::ofstream tex("Cal.tex");
    tex << std::fixed;
    tex << '\\' << "begin{table}[H]\n"
        << '\\' << "begin{center}\n"
        << '\\' << "begin{tabular}{|c|c|}\n"
        << '\\' << "hline \n"
        << "Tensione Oscilloscopio (V)\t & \t Tensione Multimetro (V)\\\\ \n "
        << '\\' << "hline \n \\hline \n";

    for (int ii = 0; ii != i; ++ii)
    {
        tex << "\\( " << v_o[ii] << " \\pm " << ev_o[ii] << "\\)"
            << "\t & \t"
            << "\\( " << v_m[ii] << " \\pm " << ev_m[ii] << "\\)"
            << "\\\\" << '\n'
            << "\\hline" << '\n';
    }
    tex << '\\' << "end{tabular}\n"
        << '\\' << "end{center}\n"
        << '\\' << "end{table}\n";
}