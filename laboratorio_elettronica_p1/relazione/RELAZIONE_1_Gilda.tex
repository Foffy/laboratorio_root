\input{../preamble.tex}
\title{Misura della Caratteristica di un Diodo a Giunzione P-N}
\author{Federico Ciampiconi, Gabriele Guarino}
\date{26/11/2021}
\begin{document}
	\maketitle
	\tableofcontents
	\newpage

\section{Abstract}



L'obbiettivo dell'esperienza è stato la misura delle caratteristiche I-V di due diodi a giunzione PN (Si, Ge) polarizzati direttamente, con adattamento dell'equazione del diodo ideale di Shockley per ottenere una stima della corrente inversa di saturazione $\mathrm{I_{0}}$ e di $\mathrm{\eta V_{T}}$.  I valori stimati per il germanio sono $\mathrm{I_{0} = 1.6^{+1.2}_{-0.7}\mu A}$ e $\mathrm{\eta V_{T} = 34^{+7}_{-6}mV}$ mentre per il silicio $\mathrm{I_{0} = 2.4^{+1.0}_{-0.8}nA}$ e $\mathrm{\eta V_{T} = 49^{+2}_{-2}mV}$.

\section{Apparato sperimentale}
\begin{wrapfigure}[14]{l}{0.4\textwidth}
	\centering
	\begin{circuitikz} \draw 
		
		(0,-2) node[ground]{} -- (0,1) (0,-1)  to [american potentiometer = \(\mathrm{1k\Omega}\),n=pot,*-]++(4,0)  to [short, -o](4,-2) node[anchor=north]{\(+5\mathbf{V}\)}
		[short, -](4,3)  to [full diode=\textbf{N-P}, diodes/scale=0.5](0,3) (pot.wiper) --(4,-0.44)to[ammeter,-*](4,1)
		(4,1)--(4,1) to[voltmeter,-*] (0,1) 
		(0.5,-2)
		(0,1)--(0,3)
		(4,1)--(4,3)
		(0,1)node[anchor=east]{A}
		(4,1)node[anchor=west]{B}
		(pot.wiper)++(2,0)node[anchor=west]{C}
		(4,-1.8)node[anchor=west]{\textbf{(1)}}
		(2.05,1.3)node[anchor=south]{\textbf{(2)}}
		(4.35,0.3)node[anchor=west]{\textbf{(3)}}
		;
	\end{circuitikz}
	\caption{Circuito realizzato}
	\label{fig:schem}
\end{wrapfigure}

Inizialmente è stato costruito il circuito in \autoref{fig:schem} sulla breadboard. Per effettuare la calibrazione della tensione misurata con l’oscilloscopio in funzione di quella misurata con il multimetro, è stato collegato l’oscilloscopio al punto C  e sono stati cortocircuitati i punti A-B.
 La tensione è stata fatta variare da 0.05V a 0.5V cambiando la resistenza del potenziometro, prendendo misure ogni 50mV e ottenendo abbastanza punti per poter effettuare un fit. Una volta appurato che gli strumenti fossero calibrati, è stato posto il diodo tra i punti A-B ed in parallelo è stato collegato l'oscilloscopio. Successivamente sono state effettuate le misure di corrente (con il multimetro) e di tensione (con l'oscilloscopio) per entrambi i diodi, ottenendo le curve caratteristiche. Dalla fase di acquisizione sono stati ottenuti 17 punti per il germanio nell'intervallo \(\mathrm{\left[60mV,200mV\right]}\) e 26 per il silicio nell'intervallo \(\mathrm{\left[360mV,760mV\right]}\). \\\\
\begin{table}[h]
	\centering
	\begin{tabular}{|l|c|c|}
		\cline{2-3}
		\multicolumn{1}{c|}{}&\autoref{fig:schem}&\textbf{Modello}\\
		\hline
	    Generatore &\textbf{(1)}&EB2025T TRIPLE OUTPUT PSU \\
	    \hline
	    Oscilloscopio &\textbf{(2)}&ISO TECH ISR622\\
	    \hline
	    Multimetro &\textbf{(3)}&ISO TECH IDM 105\\
	    \hline
	\end{tabular}
\caption{Strumentazione utilizzata}
\end{table}

\section{Risultati e Discussione}
\subsection{Fase Preliminare: Calibrazione Strumenti}

Il riferimento dell'oscilloscopio analogico è stato fissato utilizzando la scala 5mV/DIV.\\
Il fit (\autoref{fig:cal}) con una retta \(\mathrm{V_{oscilloscope} = A + BV_{multimeter}}\) non ha rivelato differenze significative tra le misure di tensione effettuate con multimetro e oscilloscopio: \(\mathrm{A = (0.000 \pm 0.003)V}\) (c.l. 95\%) risulta compatibile con 0V e \(\mathrm{B} = (1.01\pm 0.02)\) (c.l. 95\%) risulta compatibile con 1, non si è reso perciò necessario applicare alcuna correzione alle misure effettuate con l'oscilloscopio.
\begin{figure}[H]
	\centering
	\input{../build/Calt.tex}
	\caption{Retta di calibrazione Oscilloscopio-Multimetro}
	\label{fig:cal}
\end{figure}
\begin{table}[H]
	\begin{center}
		\begin{tabular}{|c|c|c|c|}
			\hline 
			Tensione Oscilloscopio (V) &  V/Div &	 Tensione Multimetro (V) & F.S. (V)\\ 
			\hline 			
			\hline
			\( 0.050 \pm 0.001\) & 0.010 & 	\( 0.0492 \pm 0.0008\) & 0.400\\
			\cline{0-2} 
			\( 0.080 \pm 0.001\) & 0.020	 & 	\( 0.0804 \pm 0.0008\)& \\
			\cline{0-0} \cline{3-3}
			\( 0.100 \pm 0.003\) & 	 & 	\( 0.0979 \pm 0.0009\)& \\
			\cline{0-2} 
			\( 0.150 \pm 0.003\) & 0.050	 & 	\( 0.150 \pm 0.001\)& \\
			\cline{0-0} \cline{3-3}
			\( 0.200 \pm 0.003\) & 	 & 	\( 0.200 \pm 0.002\)& \\
			\cline{0-0} \cline{3-3}
			\( 0.250 \pm 0.003\) & 	 & 	\( 0.249 \pm 0.002\)& \\
			\cline{0-0} \cline{3-3}
			\( 0.300 \pm 0.003\) & 	 & 	\( 0.297 \pm 0.002\)& \\
			\cline{0-0} \cline{3-3}
			\( 0.350 \pm 0.003\) & 	 & 	\( 0.347 \pm 0.004\)& \\
			\hline
			\( 0.400 \pm 0.005\) & 0.1	 & 	\( 0.396 \pm 0.004\)& 4\\
			\cline{0-0} \cline{3-3}
			\( 0.450 \pm 0.005\) & 	 & 	\( 0.446 \pm 0.004\)& \\
			\cline{0-0} \cline{3-3}
			\( 0.500 \pm 0.005\) & 	 & 	\( 0.495 \pm 0.004\)& \\
			\hline
		\end{tabular}
	\end{center}
\caption{Valori di tensione ottenuti con oscilloscopio e multimetro per la calibrazione}
\label{tab:cal}
\end{table}
\subsection{Risultati}
\begin{figure}[H]
	\begin{subfigure}{0.5\textwidth}
		\resizebox{1.05\textwidth}{0.7\textwidth}{
			\input{../IVt.tex}
		}
		\caption{Diodo al Silicio, \\fit effettuato per \(\mathrm{V \in \left[360mV,560mV\right]}\)}
		\label{fig:si}
		
	\end{subfigure}
	\begin{subfigure}{0.5\textwidth}
		\resizebox{1.05\textwidth}{0.7\textwidth}{
			\input{../build/IVt.tex}
		}
		\caption{Diodo al Germanio, \\fit effettuato per \(\mathrm{V \in \left[60mV,100mV\right]}\)}
		\label{fig:ge}
	\end{subfigure}
	
	\caption{Curve caratteristiche I-V dei diodi; scala logaritmica sull'asse verticale}
	\label{fig:car}
\end{figure}
Il comportamento dei diodi, all'aumentare della tensione di polarizzazione diretta, si discosta significativamente dal comportamento ideale; per stimare i parametri si è dunque scelto di adattare il seguente modello:
\begin{align}
&\mathrm{I(V) = A\left[\exp\left(V\cdot B\right)-1\right],} &&\mathrm{A\equiv I_0,}\ \ \mathrm{B\equiv\left[\eta V_T\right]^{-1}}
\end{align}
solamente nei punti a più basse tensioni (\autoref{fig:car}), riportati in \autoref{tab:fit}. Dall'adattamento delle curve si sono ottenute quindi le stime di \(\mathrm{I_0}\) e \(\mathrm{\eta V_T}\) riportate in \autoref{tab:fitpar}.
\begin{table}[H]
	\begin{center}
		\begin{tabular}{|c|c|c|c||c|c|c|c|}
			\hline 
			\multicolumn{4}{|c||}{\textbf{Si}} & \multicolumn{4}{c|}{\textbf{Ge}}\\\hline
			I (mA) 	 & F.S. (mA)	& V (mV) & mV/Div & I (mA) & F.S. (mA)	 & 	 V (mV) & mV/Div \\ 
			\hline 
			\hline 
			\( 0.003 \pm 0.002\)& 4	 & 	\( 360 \pm 5\) & 100 &  \( 0.008 \pm 0.002\)&	4 & 	\( 60 \pm 1\)& 10\\
			
			\cline{0-0}\cline{3-3}\cline{5-5}\cline{7-7}
			\( 0.005 \pm 0.002\)& 	 & 	\( 380 \pm 5\)&  & \( 0.010 \pm 0.002\)	& & 	\( 70 \pm 1\)&\\
			
			\cline{0-0}\cline{3-3}\cline{5-5}\cline{7-7}\cline{8-8}
			\( 0.008 \pm 0.002\)& 	 & 	\( 400 \pm 5\)&  & \( 0.015 \pm 0.002\)&	 & 	\( 80 \pm 1\)&20\\
			
			\cline{0-0}\cline{3-3}\cline{5-5}\cline{7-7}
			\( 0.010 \pm 0.002\)& 	 & 	\( 410 \pm 5\)&  & \( 0.017 \pm 0.002\)&	 & 	\( 84 \pm 1\)&\\
			
			\cline{0-0}\cline{3-3}\cline{5-5}\cline{7-7}
			\( 0.012 \pm 0.002\)& 	 & 	\( 420 \pm 5\)&  & \( 0.019 \pm 0.002\)&	 & 	\( 88 \pm 1\)&\\
			
			\cline{0-0}\cline{3-3}\cline{5-5}\cline{7-7}
			\( 0.015 \pm 0.002\)& 	 & 	\( 430 \pm 5\)&  & \( 0.021 \pm 0.002\)&	 & 	\( 92 \pm 1\)&\\
			
			\cline{0-0}\cline{3-3}\cline{5-5}\cline{7-7}
			\( 0.018 \pm 0.002\)& 	 & 	\( 440 \pm 5\)&  & \( 0.024 \pm 0.002\)	& & 	\( 96 \pm 1\)&\\
			
			\cline{0-0}\cline{3-3}\cline{5-5}\cline{7-7}
			\( 0.022 \pm 0.002\)& 	 & 	\( 450 \pm 5\)&  & \( 0.028 \pm 0.002\)&	 & 	\( 100 \pm 1\)&\\
			
			\cline{0-0}\cline{3-3}\cline{5-8}
			\( 0.028 \pm 0.002\)& 	 & 	\( 460 \pm 5\)&  & \multicolumn{4}{c}{}\\
			\cline{0-0}\cline{3-3}
			\( 0.033 \pm 0.002\)& 	 & 	\( 470 \pm 5\)&  & \multicolumn{4}{c}{}\\
			\cline{0-0}\cline{3-3}
			\( 0.042 \pm 0.002\)& 	 & 	\( 480 \pm 5\)&  & \multicolumn{4}{c}{}\\
			\cline{0-0}\cline{3-3}
			\( 0.050 \pm 0.002\)& 	 & 	\( 490 \pm 5\)&  & \multicolumn{4}{c}{}\\
			\cline{0-0}\cline{3-3}
			\( 0.063 \pm 0.002\)& 	 & 	\( 500 \pm 5\)&  & \multicolumn{4}{c}{}\\
			\cline{0-0}\cline{3-3}
			\( 0.073 \pm 0.002\)& 	 & 	\( 510 \pm 5\)&  & \multicolumn{4}{c}{}\\
			\cline{0-0}\cline{3-3}
			\( 0.094 \pm 0.002\)& 	 & 	\( 520 \pm 5\)&  & \multicolumn{4}{c}{}\\
			\cline{0-0}\cline{3-3}
			\( 0.109 \pm 0.002\)& 	 & 	\( 530 \pm 5\)&  & \multicolumn{4}{c}{}\\
			\cline{0-0}\cline{3-3}
			\( 0.139 \pm 0.003\)& 	 & 	\( 540 \pm 5\)&  & \multicolumn{4}{c}{}\\
			\cline{0-0}\cline{3-3}
			\( 0.205 \pm 0.003\)& 	 & 	\( 560 \pm 5\)&  & \multicolumn{4}{c}{}\\
			\cline{0-3}\cline{3-3}
			
		\end{tabular}
	\caption{Punti delle Caratteristiche utilizzati nell'adattamento delle curve}
	\label{tab:fit}
	\end{center}
\end{table}
\begin{table}[H]
	\centering
	\begin{tabular}{|r|c|c|c|c|c|c|}
			\cline{2-7}
		\multicolumn{1}{c|}{} & \multicolumn{3}{c|}{\textbf{Si}} & \multicolumn{3}{c|}{\textbf{Germanio}}\\
		
		\multicolumn{1}{c|}{} & \multicolumn{1}{c}{\textbf{Stima}} &\multicolumn{1}{c}{c.l. 68\%}& \multicolumn{1}{c|}{c.l. 95\%} & \multicolumn{1}{c}{\textbf{Stima}}& \multicolumn{1}{c}{c.l. 68\%}& \multicolumn{1}{c|}{c.l. 95\%} \\\hline
		
		\(\mathrm{\eta V_T(mV)}\) & $49$ &\(\left[48,51\right]\)& \(\left[46,53\right]\) & \(34\) & \(\left[28,41\right]\) & \(\left[22,48\right]\)\\\hline
		
		\(\mathrm{I_0(mA)}\) &\(2.4\cdot 10^{-6}\) & \([1.7,3.5]\cdot10^{-6}\) & \( [1.1,4.9]\cdot10^{-6}\) & \(1.6\cdot10^{-3}\) &\([0.9,2.8]\cdot10^{-3}\)&\([0.5,4.9]\cdot 10^{-3}\)\\\hline
	\end{tabular}
\caption{Parametri ottenuti dai fit}
\label{tab:fitpar}
\end{table}

\section{Conclusioni}
\begin{table}[H]
	\centering
	\begin{tabular}{|c|c|c|c|c|}
		\cline{2-5}
		\multicolumn{1}{c|}{} & \multicolumn{2}{c|}{\textbf{Si}} & \multicolumn{2}{c|}{\textbf{Germanio}}\\
		\multicolumn{1}{c|}{} & \multicolumn{1}{c}{c.l. 95\%} & \multicolumn{1}{c|}{\textbf{\textbf{Valore Atteso \(\sim300\mathrm{K}\)}}} & \multicolumn{1}{c}{c.l. 95\%} & \multicolumn{1}{c|}{\textbf{\textbf{Valore Atteso} \(\sim300\mathrm{K}\)}} \\\hline
		\(\mathrm{\eta V_T(mV)}\) & \(\left[46,53\right]\) & \(\sim52\) &  \(\left[22,48\right]\)& \(\sim26\) \\\hline
		\(\mathrm{I_0(mA)}\) & \( [1.1,4.9]\cdot10^{-6}\) & \(\sim10^{-6}\) & \([0.5,4.9]\cdot 10^{-3}\)& \(\sim10^{-3}\)\\\hline
	\end{tabular}
\end{table}
Negli intervalli di adattamento delle curve non risultano discrepanze significative, si hanno infatti valori di \(\chi^2\) estremamente piccoli tanto che \(P(\chi^2_\nu > \chi^2) \approx 1\), questo suggerisce una sovrastima delle incertezze; le stime dei parametri risultano compatibili con i valori attesi, tuttavia si evidenzia che se le incertezze sulle misure fossero sovrastimate lo sarebbero anche quelle sui parametri\cite{Minos}. In conclusione non sono state rivelate discrepanze tra il modello e i dati.
\appendix
\section{Stima delle Incertezze}
\subsection{Multimetro ISO TECH IDM 105}
Le incertezze sulle misure di tensione e corrente (in regime di corrente continua) sono state stimate secondo quanto riportato nelle specifiche del costruttore\cite{105} e sono riportate in \autoref{tab:emm}:
\begin{table}[H]
	\centering
	\begin{tabular}{|rl|rl|}
		\hline
		\multicolumn{2}{|c|}{\textbf{DCV}} & \multicolumn{2}{c|}{\textbf{DCA}}\\
		
		Fondo Scala & Incertezza(\(\pm\)) & Fondo Scala & Incertezza(\(\pm\)) \\
		\hline
		\hline
		400mV & \(0.3\% + 0.2\mathrm{mV}\) & 4mA & \(0.4\% + 2\mu\mathrm{A}\)\\
		\hline
		4V & \(0.1\% + 2\mathrm{mV}\) & 40mA & \(0.4\% + 20\mu\mathrm{A}\) \\
		\hline
		\multicolumn{2}{c|}{}& 400mA & \(0.4\% + 200\mu\mathrm{A}\) \\\cline{3-4}
		
	\end{tabular}
	\caption{Stime delle incertezze e relativo fondo scala}
\label{tab:emm}
\end{table}
\subsection{Oscilloscopio ISO TECH ISR622}
L'incertezza sulle misure di tensione ottenute con l'oscilloscopio sono state stimate sommando in quadratura gli errori di lettura e la sensibilità dello strumento (\(\sim\mathrm{1mV}\))\cite{osc} riportata dal costruttore:
\begin{equation}
	\delta\mathrm{V} = \pm\sqrt{\left(\frac{\mathrm{V/Div}_\mathrm{mis}}{20}\right)^2 + 
	\left(\frac{\mathrm{V/Div}_\mathrm{GND}}{20}\right)^2 + \left(\mathrm{1mV}\right)^2	
}
\end{equation}
Dove \(\mathrm{V/Div_{mis}}\) e \(\mathrm{V/Div_{GND}}\) sono i valori di Volt per divisione utilizzati rispettivamente per ottenere ciascuna misura e fissare il riferimento dell'oscilloscopio. Come risoluzione si è scelto di utilizzare 1/4 di sottodivisione, ovvero \(\mathrm{\frac{V/Div}{20}}\).
\subsection{Parametri Fit}
I parametri dei fit sono stati stimati tramite minimizzazione numerica di \[\chi^2 =\sum_{i}{ \frac{\left[y_i-f(x_i;\vec{\alpha})\right]^2}{\sigma_{y_i}^2 + \frac{1}{4}\left(f(x_i+\sigma_{x_i})-f(x_i-\sigma_{x_i})\right)^2}}\] eseguita da ROOT; le incertezze associate in \autoref{fig:car} e \autoref{fig:cal} sono state stimate come segue\cite{NumRec}: 
\begin{equation}
	\mathrm{\delta \alpha_i = \pm\sqrt{\Delta\chi^2_\nu H_{ii}^{-1}}}
\end{equation}
dove \(\mathrm{H_{ij} = \frac{\partial^2\chi^2}{\partial \alpha_i \partial \alpha_j}}\), \(\mathrm{\alpha_i}\) è il vettore dei parametri e \(\Delta\chi^2_\nu\) è la variazione di \(\chi^2_\nu\) associata ad un livello di confidenza del 68\%(\(1\sigma\)). Gli intervalli di confidenza dei parametri riportati in \autoref{tab:fitpar} sono stati stimati da ROOT tramite il metodo MINOS ERRORS e in generale non sono simmetrici rispetto la stima del parametro\cite{Minos}.

\subsection{\(\eta\mathrm{V_T}\)}
Il modello adattato ai dati sperimentali è \(\mathrm{I(V) = A\left(\exp\left(V\cdot B\right) - 1\right)}\) dove \(\mathrm{A \equiv I_{0}}\) e \(\mathrm{B\equiv\left[\eta V_T\right]^{-1}}\), pertanto propagando le incertezze risulta:
\[\delta\eta\mathrm{V_T} = \frac{\delta\mathrm{B}}{\mathrm{B^2}}\]
\bibliography{refs.bib}{}
\bibliographystyle{siam}




\end{document}
