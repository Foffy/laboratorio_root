# LABORATORIO DI ELETTRONICA: PRIMA PROVA
## calibrazione
inserire i dati di calibrazione in: build/data_cal.dat
## caratteristica
inserire i dati della curva caratteristica del diodo: build/data_diodo.dat
## Build
Per costruire il progetto:
```shell
$ cd Path/to/Repository/laboratorio_elettronica_p1/build
$ cmake -DCMAKE_BUILD_TYPE:STRING=Release .. 
$ cmake --build . --target all -j 10
```

## Dependecies
Il progetto necessita, per quanto riguarda la parte di elaborazione dati, di un compilatore C++ che supporti lo standard C++20 e di una versione recente di ROOT. 

