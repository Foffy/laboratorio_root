#include "TF1.h"
#include "TGraphErrors.h"
#include "TAxis.h"
#include "TCanvas.h"
#include "TStyle.h"
#include "Math/Minimizer.h"
#include "TFitResult.h"
#include "TFitResultPtr.h"
#include "uncertainty_generation/egen.hxx"
#include <fstream>
#include <iostream>

int main()
{
    gStyle->SetOptTitle(1);
    gStyle->SetOptStat(10);
    gStyle->SetOptFit(1110);
    gStyle->SetStatY(0.28);
    gStyle->SetStatX(0.9);
    gStyle->SetStatW(0.18);
    gStyle->SetStatH(0.18);
    ROOT::Math::MinimizerOptions::SetDefaultErrorDef(1.);

    Double_t w = 3000;
    Double_t h = 2000;
    TCanvas *canvas = new TCanvas();
    canvas->SetTitle("Calibrazione");
    canvas->SetWindowSize(w, h);
    canvas->SetGridx();
    canvas->SetGridy();
    canvas->SetLogy();

    std::ifstream data;
    data.open("data_diodo.dat");
    int i = 0;
    double v;
    while (data >> v)
    {
        ++i;
    }
    i -= 2;

    data.close();
    if ((i % 4) != 0)
    {
        throw std::logic_error("Wrong Data Format");
    }
    data.open("data_diodo.dat");
    i = i / 4;
    std::cout << i << '\n';

    double min = 0;
    double max = 0;
    data >> min >> max;
    min *= 1e3;
    max *= 1e3;
    std::cout << "Fit Range: [" << min << ", " << max << "]\n";
    double I[i];
    double eI[i];
    double V[i];
    double eV[i];
    for (int ii = 0; ii != i; ++ii)
    {
        data >> I[ii] >> eI[ii] >> V[ii] >> eV[ii];
        eI[ii] = 1e3 * feI(I[ii], eI[ii]);
        eV[ii] = 1e3 * feV(V[ii], eV[ii]);
        I[ii] *= 1e3;
        V[ii] *= 1e3;
    }
    std::cout << I[0] << '\n';
    TGraphErrors diodo(i, V, I, eV, eI);
    TF1 ivfun("Cal", "[0] * TMath::Exp(x * [1]) - [0]", min, max);
    ////////////////////////////
    ////    SETTING STYLE   ////
    ////////////////////////////
    ivfun.SetNpx(1e3);
    ivfun.SetLineColorAlpha(kBlue, 0.5);
    ivfun.SetLineWidth(2);
    diodo.SetMarkerStyle(8);
    diodo.SetMarkerSize(1.2);

    // TF1 ivfun("Cal", "[1] * (exp(x",min,max);
    ivfun.SetParameter(1, 1 / 45.); //[etaVt]^-1
    ivfun.SetParameter(0, 1.75e-3); // I0
    ivfun.SetParLimits(1, 0., 1. / 10.);
    // ivfun.SetParLimits(1, 1, 100);
    // ivfun.SetParameter(0,1.);
    ivfun.SetParName(0, "#mathrm{I_{0}}");
    ivfun.SetParName(1, "#left[#eta V_{T}#right]^{-1}");
    diodo.SetTitle("Caratteristica Diodo");
    diodo.GetXaxis()->SetTitle("V (mV)");
    diodo.GetYaxis()->SetTitle("I (mA)");
    auto p = diodo.Fit(&ivfun, "R,M,E,S");
    auto pr = p.Get();
    auto cm = pr->GetCovarianceMatrix();
    cm.Print();
    std::cout << "I0 interval: " << ivfun.GetParameter(0) << '\t' << ivfun.GetParameter(0) + pr->LowerError(0) << "\t" << ivfun.GetParameter(0) + pr->UpperError(0) << '\n';
    std::cout << "I0 interval errors: " << pr->LowerError(0) << "\t" << pr->UpperError(0) << '\n';
    std::cout << "etaVt: " << 1. / ivfun.GetParameter(1) << '\t' << "min " << 1. / ivfun.GetParameter(1) + pr->LowerError(1) / (ivfun.GetParameter(1) * ivfun.GetParameter(1)) << "\t"
              << "max " << 1. / ivfun.GetParameter(1) + pr->UpperError(1) / (ivfun.GetParameter(1) * ivfun.GetParameter(1)) << '\n';
    std::cout << "etaVt errors: "
              << "min " << pr->LowerError(1) / (ivfun.GetParameter(1) * ivfun.GetParameter(1)) << "\t"
              << "max " << pr->UpperError(1) / (ivfun.GetParameter(1) * ivfun.GetParameter(1)) << '\n';

    TF1 ivfunc(ivfun);
    ivfunc.SetRange(0., 1e3);
    ivfunc.SetNpx(50);
    ivfunc.SetLineStyle(kDashed);
    diodo.Draw("AP");
    ivfunc.Draw("L SAME");

    canvas->Print("IV.png");
    canvas->Print("IVt.tex");
    std::ofstream tex("IV.tex");
    tex << std::fixed;
    tex << '\\' << "begin{table}[H]\n"
        << '\\' << "begin{center}\n"
        << '\\' << "begin{tabular}{|c|c|}\n"
        << '\\' << "hline \n"
        << "I (mA) \t & \t V (mV) \\\\ \n "
        << '\\' << "hline \n \\hline \n";

    for (int ii = 0; ii != i; ++ii)
    {
        tex << "\\( " << I[ii] << " \\pm " << eI[ii] << "\\)"
            << "\t & \t"
            << "\\( " << V[ii] << " \\pm " << eV[ii] << "\\)"
            << "\\\\" << '\n'
            << "\\hline" << '\n';
    }
    tex << '\\' << "end{tabular}\n"
        << '\\' << "end{center}\n"
        << '\\' << "end{table}\n";
}