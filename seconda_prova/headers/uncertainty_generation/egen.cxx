/**
 * @file egen.cxx
 * @author Federico Ciampiconi (federico.ciampiconi@studio.unibo.it)
 * @brief
 * @version 0.1
 * @date 2021-12-11
 *
 * @copyright Copyright (c) 2021
 *
 */
#include "egen.hxx"
#include <math.h>
#define SIGMA2GND 6.25e-8
double feV(double v, double vdiv)
{
    return sqrt((vdiv / 20.) * (vdiv / 20.) + SIGMA2GND + 1e-6 + v*v*0);
}
double femV(double v, double max)
{
    double uncertainty = 0.;
    if (max == 0.4)
        uncertainty = v * 0.0075 + 2e-4;
    else if (max == 4)
        uncertainty = v * 0.005 + 2e-3;
    return uncertainty;
}
double feI(double v, double max)
{
    double uncertainty = 0.;
    if (max == 4e-3)
        uncertainty = v * 0.004 + 2e-6;
    else if (max == 4e-2)
        uncertainty = v * 0.004 + 2e-5;
    else if (max == 4e-1)
        uncertainty = v * 0.004 + 2e-4;
    return uncertainty;
}