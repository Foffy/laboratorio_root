/**
 * @file bjt.cxx
 * @author Federico Ciampiconi (federico.ciampiconi@studio.unibo.it)
 * @brief
 * @version 0.1
 * @date 2021-12-11
 *
 * @copyright Copyright (c) 2021
 *
 */
#include "uncertainty_generation/egen.hxx"
#include "TF1.h"
#include "TGraphErrors.h"
#include "TAxis.h"
#include "TCanvas.h"
#include "TStyle.h"
#include "TLegend.h"
#include "TH1F.h"
#include "TMarker.h"
#include "TFitResult.h"
#include "TFitResultPtr.h"
#include <fstream>
#include <iostream>

int main()
{
    /////////////////////////////////////
    ////////////Setting Style////////////
    /////////////////////////////////////
    gStyle->SetOptTitle(1);
    gStyle->SetOptStat(10);
    gStyle->SetOptFit(1110);
    gStyle->SetStatY(0.28);
    gStyle->SetStatX(0.9);
    gStyle->SetStatW(0.18);
    gStyle->SetStatH(0.18);
    /////////////////////////////////////
    ///////////Creating Canvas///////////
    /////////////////////////////////////
    TCanvas canvas;
    canvas.SetWindowSize(3000, 2000);
    canvas.SetGridx();
    canvas.SetGridy();

    double deltaI100 = feI(100e-6, 4e-3);
    double deltaI200 = feI(200e-6, 4e-3);
    std::ifstream data("data/data.dat");
    unsigned int N = 39U;

    double aq1[4][N]; // 200uA
    double aq2[4][N]; // 100uA
    double fs[2][N];
    double vdiv[2][N];

    unsigned int t = 0;
    std::string a;
    while (data >> a)
    {
        ++t;
    }
    data.close();
    std::cout << "Data Points:\t" << t / 4 << '\n';
    if ((t % (N * 8U)) != 0U && (t != 0))
    {
        // ensures 4 values are given
        // for each data point of 200uA and 100uA Acquisitions
        //&& there are N data points
        throw std::logic_error("Wrong Data Format");
    }
    data.open("data/data.dat");
    std::cout << std::fixed;
    std::cout << "\tV\t\tI\t\teV\t\teI\t\t\tV\t\tI\t\teV\t\teI" << '\n';
    for (unsigned int n = 0; n != N; ++n)
    {
        std::cout << "AQ1:\t";
        for (int d = 0; d != 4; ++d)
        {
            data >> aq1[d][n];

            if (d == 2)
            {
                vdiv[0][n] = aq1[d][n];
                aq1[d][n] = feV(abs(aq1[d - 2][n]), aq1[d][n]);
            }
            else if (d == 3)
            {
                fs[0][n] = aq1[d][n];
                aq1[d][n] = feI(abs(aq1[d - 2][n]), aq1[d][n]);
            }
            std::cout << aq1[d][n] << '\t';
        }
        std::cout << "AQ2:\t";
        for (int d = 0; d != 4; ++d)
        {
            data >> aq2[d][n];

            if (d == 2)
            {
                vdiv[1][n] = aq2[d][n];
                aq2[d][n] = feV(abs(aq2[d - 2][n]), aq2[d][n]);
            }
            else if (d == 3)
            {
                fs[1][n] = aq2[d][n];
                aq2[d][n] = feI(abs(aq2[d - 2][n]), aq2[d][n]);
            }
            std::cout << aq2[d][n] << '\t';
        }
        aq1[0][n] = -aq1[0][n];
        aq1[1][n] = -1000 * aq1[1][n];
        aq1[3][n] = 1000 * aq1[3][n];
        aq2[0][n] = -aq2[0][n];
        aq2[1][n] = -1000 * aq2[1][n];
        aq2[3][n] = 1000 * aq2[3][n];
        std::cout << '\n';
    }

    TGraphErrors gAQ1(N, aq1[0], aq1[1], aq1[2], aq1[3]);
    TGraphErrors gAQ2(N, aq2[0], aq2[1], aq2[2], aq2[3]);
    /////////////////////////////////////
    ///////////Creating tables///////////
    /////////////////////////////////////
    std::ofstream tex1("table.tex");
    tex1.precision(4);
    tex1 << std::fixed;
    tex1 << '\\' << "begin{table}[H]\n"
         << '\\' << "begin{center}\n"
         << '\\' << "begin{tabular}{|c|c|c|c||c|c|c|c|}\n"
         << '\\' << "hline \n"
         << "\\multicolumn{4}{|c||}{\\textbf{\\(\\mathrm{I_B = -200\\mu A}\\)}} & \\multicolumn{4}{c|}{\\textbf{\\(\\mathrm{I_B = -100\\mu A}\\)}}\\\\\n\\hline\n"
         << "V (V) \t &\t V/Div \t& \t I (mA)\t & F.S. (mA) \t&\t V (V) \t &\t V/Div \t& \t I (mA)\t & F.S. (mA) \\\\ \n "
         << '\\' << "hline \n \\hline \n";
    {
        bool newvdiv1 = true;
        bool newfs1 = true;
        bool newvdiv2 = true;
        bool newfs2 = true;
        for (unsigned int ii = 0; ii != N; ++ii)
        {

            tex1 
                 << (newvdiv1  ? "\\cline{0-1}\t" : "\\cline{0-0}\t") << '\t'
                 << (newfs1  ? "\\cline{3-4}\t" : "\\cline{3-3}\t") << '\t'
                 << (newvdiv2  ? "\\cline{5-6}\t" : "\\cline{5-5}\t") << '\t'
                 << (newfs2  ? "\\cline{7-8}\t" : "\\cline{7-7}\t") << '\n'
                 << "\\( " << aq1[0][ii] << " \\pm " << aq1[2][ii] << "\\)"
                 << "\t & \t"
                 << "\\(" << (newvdiv1 ? std::to_string(vdiv[0][ii]) : "") << "\\)"
                 << "\t & \t"
                 << "\\( " << aq1[1][ii] << " \\pm " << aq1[3][ii] << "\\)"
                 << "\t & \t"
                 << "\\(" << (newfs1 ? std::to_string(fs[0][ii] * 1e3) : "") << "\\)"
                 << "\t & \t"
                 << "\\( " << aq2[0][ii] << " \\pm " << aq2[2][ii] << "\\)"
                 << "\t & \t"
                 << "\\(" << (newvdiv2 ? std::to_string(vdiv[1][ii]) : "") << "\\)"
                 << "\t & \t"
                 << "\\( " << aq2[1][ii] << " \\pm " << aq2[3][ii] << "\\)"
                 << "\t & \t"
                 << "\\(" << (newfs2 ? std::to_string(fs[1][ii] * 1e3) : "") << "\\)"
                 << "\\\\" << '\n';
                
                 
            if (ii != N - 1)
            {
                newfs1 = fs[0][ii] != fs[0][ii + 1];
                newvdiv1 = vdiv[0][ii] != vdiv[0][ii + 1];
                newfs2 = fs[1][ii] != fs[1][ii + 1];
                newvdiv2 = vdiv[1][ii] != vdiv[1][ii + 1];
            }
        }
        tex1 << "\\hline\n";
    }
    tex1 << '\\' << "end{tabular}\n"
         << '\\' << "end{center}\n"
         << '\\' << "end{table}\n";
    std::ofstream tex2("table2.tex");
    tex2 << std::fixed;
    tex2 << '\\' << "begin{table}[H]\n"
         << '\\' << "begin{center}\n"
         << '\\' << "begin{tabular}{|c|c|}\n"
         << '\\' << "hline \n"
         << "V (V) \t & \t I (mA) \\\\ \n "
         << '\\' << "hline \n \\hline \n";

    for (unsigned int ii = 0; ii != N; ++ii)
    {
        tex2 << "\\( " << aq2[0][ii] << " \\pm " << aq2[2][ii] << "\\)"
             << "\t & \t"
             << "\\( " << aq2[1][ii] << " \\pm " << aq2[3][ii] << "\\)"
             << "\\\\" << '\n'
             << "\\hline" << '\n';
    }
    tex2 << '\\' << "end{tabular}\n"
         << '\\' << "end{center}\n"
         << '\\' << "end{table}\n";
    /////////////////////////////////////
    gAQ1.SetMarkerStyle(8);
    gAQ1.SetMarkerSize(1.2);
    gAQ1.SetLineWidth(2);
    gAQ1.SetMarkerColor(kRed);
    gAQ1.SetLineColor(kRed);
    gAQ1.SetTitle("");
    gAQ2.SetMarkerStyle(8);
    gAQ2.SetMarkerSize(1.2);
    gAQ2.SetLineWidth(2);
    gAQ2.SetMarkerColor(kBlue);
    gAQ2.SetLineColor(kBlue);
    gAQ2.SetTitle("");
    gAQ1.GetXaxis()->SetTitle("#mathrm{#left|V_{CE}#right| (V)}");
    gAQ1.GetYaxis()->SetTitle("#mathrm{#left|I_{C}#right| (mA)}");
    gAQ2.GetXaxis()->SetTitle("#mathrm{#left|V_{CE}#right| (V)}");
    gAQ2.GetYaxis()->SetTitle("#mathrm{#left|I_{C}#right| (mA)}");
    ////////////////////////////////////
    TLegend leg(0.1, 0.75, 0.35, 0.9);
    leg.AddEntry(&gAQ1, "#mathrm{I_{B} = -200#mu A}");
    leg.AddEntry(&gAQ2, "#mathrm{I_{B} = -100#mu A}");
    ///////////////////////////////////
    gAQ1.Draw("ap");
    gAQ2.Draw("p same");
    leg.Draw();

    canvas.Print("curves.png");
    canvas.Print("curves.tex");
    canvas.Clear();

    ///////////////////////////////////
    ////////////////FIT////////////////
    ///////////////////////////////////
    TF1 l("", "pol1", 0, 5);
    l.SetNpx(20);
    l.SetLineColorAlpha(kRed, .4);
    l.SetLineStyle(kDashed);
    l.SetLineWidth(2);
    l.SetParNames("Intercept", "Angular Coefficient");
    l.SetTitle("Fitted Line");
    TF1 l1("#mathrm{200#muA}", "pol1", 1, 4);
    l1.SetLineColorAlpha(kRed, 0.6);
    l1.SetLineWidth(2);
    l1.SetParNames("Intercept", "Angular Coefficient");
    l1.SetTitle("Fitted Line");
    TF1 l2("#mathrm{100#muA}", "pol1", 1, 4);
    l2.SetLineColorAlpha(kBlue, 0.6);
    l2.SetLineWidth(2);
    l2.SetParNames("Intercept", "Angular Coefficient");
    l2.SetTitle("Fitted Line");
    auto pAQ1 = gAQ1.Fit(&l1, "R,M,E,S");
    auto pAQ2 = gAQ2.Fit(&l2, "R,M,E,S");

    TMarker intercept;
    intercept.SetMarkerStyle(70);
    intercept.SetMarkerSize(3);
    intercept.SetX(0);
    intercept.SetY(l1.Eval(0));

    l.SetParameters(l1.GetParameters());
    gAQ1.Draw("ap");
    l.Draw("l same");
    intercept.Draw();
    canvas.Print("Ib200uA.png");
    canvas.Print("Ib200uA.tex");
    canvas.Clear();
    gAQ2.Draw("ap");
    l.Draw("l same");
    l.SetParameters(l2.GetParameters());
    l.SetLineColorAlpha(kBlue, 0.5);
    TMarker intercept1;
    intercept1.SetMarkerStyle(70);
    intercept1.SetMarkerSize(3);
    intercept1.SetX(0);
    intercept1.SetY(l2.Eval(0));
    intercept1.Draw();
    canvas.Print("Ib100uA.png");
    canvas.Print("Ib100uA.tex");
    canvas.Clear();
    l1.SetRange(-1, 4);
    l2.SetRange(-1, 4);
    l1.SetMinimum(0);
    l1.Draw("l");
    l2.Draw("al same");
    unsigned int mis = 5U;
    auto rAQ1 = pAQ1.Get();
    auto rAQ2 = pAQ2.Get();
    // AQ1 Fit
    std::cout << "Intercept Ib200uA:\t" << l1.GetParameter(0) << "\tpm\t" << l1.GetParError(0) << '\n'
              << "Angular Coefficient Ib200uA:\t" << l1.GetParameter(1) << "\tpm\t" << l1.GetParError(1) << '\n'
              << "Intercept Ib100uA:\t" << l2.GetParameter(0) << "\tpm\t" << l2.GetParError(0) << '\n'
              << "Angular Coefficient Ib100uA:\t" << l2.GetParameter(1) << "\tpm\t" << l2.GetParError(1) << '\n';
    std::cout << "Resistenza in Uscita (kOhm) (200uA): \n"
              << 1. / l1.GetParameter(1) << "\tpm\t" << l1.GetParError(1) / (l1.GetParameter(1) * l1.GetParameter(1)) << '\n'
              << "Tensione di Early V_a (200uA): \n"
              << l1.GetParameter(0) / l1.GetParameter(1) << "\tpm\t" << (l1.GetParError(0) / l1.GetParameter(0) + l1.GetParError(1) / l1.GetParameter(1)) * l1.GetParameter(0) / l1.GetParameter(1) << '\n'
              << "Resistenza in Uscita (kOhm) (100uA): \n"
              << 1. / l2.GetParameter(1) << "\tpm\t" << l2.GetParError(1) / (l2.GetParameter(1) * l2.GetParameter(1)) << '\n'
              << "Tensione di Early V_a (100uA): \n"
              << l2.GetParameter(0) / l2.GetParameter(1) << "\tpm\t" << (l2.GetParError(0) / l2.GetParameter(0) + l2.GetParError(1) / l2.GetParameter(1)) * l2.GetParameter(0) / l2.GetParameter(1) << '\n';
    std::cout << "Guadagno di Corrente: \n"
              << (aq1[1][mis] - aq2[1][mis]) / 0.1 << "\tpm\t" << ((deltaI100 + deltaI200) / 100e-6 + (aq1[3][mis] + aq2[3][mis]) / (aq1[1][mis] - aq2[1][mis])) * (aq1[1][mis] - aq2[1][mis]) / 0.1 << '\n'
              << "Tensione considerata:\t" << aq1[0][mis] << '\n';
    std::cout << "Errore sulle Correnti di Base:\n"
              << "100uA:\t" << deltaI100 * 1e3 << "mA\n"
                                                  "200uA:\t"
              << deltaI200 * 1e3 << "mA\n";
}
