\input{../preamble.tex}
\title{Misura della Caratteristica in Uscita di un Transistor BJT pnp in Configurazione a Emettitore Comune}
\author{Federico Ciampiconi, Gabriele Guarino}
\date{10/12/2021}
\begin{document}
	\maketitle
	\tableofcontents
	\newpage

\section{Abstract}



Lo scopo dell'esperimento è stato la misura della caratteristica in uscita di un transistor BJT pnp al silicio in configurazione a emettitore comune: è stata misurata la corrente di collettore $\mathrm{I_{C}}$ in funzione di $\mathrm{V_{CE}}$ con due correnti di base $\mathrm{I_{B}}$ di $\mathrm{-100\mu A}$ e $-200\mathrm{\mu A}$. Tramite l'adattamento di una retta sulla caratteristica nella regione attiva ($|\mathrm{V_{CE}}|$ $\gtrsim$1V), è stata ottenuta una stima della tensione di Early $\mathrm{V_{A}}$, la resistenza di uscita b, la conduttanza di uscita g e il guadagno di corrente $\beta$ per un valore fissato di $\mathrm{V_{CE}}$. 

\section{Apparato sperimentale}
\begin{table}[H]
	\centering
	\begin{tabular}{|l|c|c|}
		\cline{2-3}
		\multicolumn{1}{c|}{}&\autoref{fig:schem}&\textbf{Modello}\\
		\hline
		Generatore &\textbf{(1)}&EB2025T TRIPLE OUTPUT PSU \\
		\hline
		Oscilloscopio &\textbf{(2)}&ISO TECH ISR622\\
		\hline
		Multimetro &\textbf{(3)}&ISO TECH IDM 105\\
		\hline
		Transistor BJT & \textbf{(4)} & 2N3906(BU) (Si PNP) \\\hline
	\end{tabular}
	\caption{Strumentazione utilizzata}
\end{table}

\begin{wrapfigure}[15]{l}{0.5\textwidth}
	\centering
	
	\begin{circuitikz} 
		
		\draw
		
		(-4,2)--(-4,-4) node[ground, anchor=north]{} (0,0)node[pnp, anchor=B, tr circle,xscale=-1](pnp){}
		(2,2)to[short,-o](2,-4)node[anchor=north]{-5V} node[anchor=east]{\textbf{(1)}}
		 (pnp.E)++(-3.16,0)to[short,*-](pnp.E)
		(2,2)to[potentiometer,n=pot1, wiper pos=0.5,n=100k](-2,2)--(-4,2) (100k.s)node[anchor=south]{\(\mathrm{100k\Omega}\)}(-2,2)
		(-2,-3)to[potentiometer=\(\mathrm{1k\Omega}\),n=1k,-*](2,-3) (-2,-3)to[short,-*](-4,-3)
		(pnp.B)to[short,i^>=$\mathrm{I_B}$](100k.wiper)
		(1k.wiper)++(-0.84,0) to[qiprobe,n=mm](pnp.C) (mm.n) node[anchor=east]{\textbf{(3)}}
		
		(1k.wiper)++(-0.84,0)--(1k.wiper)
		(pnp.C)++(-3.16,0)to[oscope,n=oscope,*-*](pnp.C)
		(oscope.n)node[anchor=south]{\textbf{(2)}}
		(oscope.w)++(-0.5,0)node[anchor=south]{\(\mathrm{V_{CE}}\)}
		(pnp.C)node[anchor=west]{C}
		(pnp.E)node[anchor=south]{E}
		(pnp)
		++(-1.1,0)node[anchor=west]{\textbf{(4)}}
		(pnp.B)node[anchor=west]{B}
		++(0,1.5)node[anchor=west]{D}
		(mm.w)++(0,-0.2)node[anchor=west]{A}
		(mm.s)node[anchor=west]{\(\mathrm{I_C}\)}

		
		;
	\end{circuitikz}
	\caption{Circuito realizzato}
	\label{fig:schem}
\end{wrapfigure}


Inizialmente è stato costruito il circuito in \autoref{fig:schem} sulla breadboard. Sono stati cortocircuitati i punti A e C e si è collegato il multimetro tra i punti B e D. Successivamente si è agito sul potenziometro da $\mathrm{100k\Omega}$ in modo da fissare la corrente di base $\mathrm{I_B}$ a $\mathrm{-200\mu A}$ (questo procedimento è stato poi ripetuto per prendere le misure della caratteristica con una corrente di base di $\mathrm{-100\mu A}$). Dunque sono stati cortocircuitati i punti B e D, è stato collegato il multimetro tra i punti A e C e l'oscilloscopio al collettore (punto C). Quindi è stata misurata la caratteristica di uscita del BJT ovvero la corrente di collettore $\mathrm{I_C}$ in funzione della tensione tra
collettore ed emettitore $\mathrm{V_{CE}}$ agendo sul potenziometro da $\mathrm{1k\Omega}$ in modo che la $\mathrm{V_{CE}}$ variasse tra -0.05V e -4V.
\newpage
\section{Risultati e Discussione}
\subsection{Dati Acquisiti}
\input{../table1.tex}
\subsection{Elaborazione Dati}
	\begin{figure}[H]
	\centering
		\input{../curves.tex}
	\caption{Risultato di entrambe le acquisizioni}
	\label{fig:cal}
\end{figure}
\begin{figure}[H]
	
	\begin{subfigure}[c][][c]{0.5\textwidth}
		\centering
		\resizebox{1.05\textwidth}{0.7\textwidth}{
			\input{../Ib100uA.tex}
		}
		\caption{Corrente di base \(\mathrm{I_B = -100\mu A}\)}
		\label{fig:si}
		
	\end{subfigure}
	\begin{subfigure}[c][][c]{0.5\textwidth}
		\centering
		\resizebox{1.05\textwidth}{0.7\textwidth}{
			\input{../Ib200uA.tex}
		}
		\caption{Corrente di base \(\mathrm{I_B = -200\mu A}\)}
		\label{fig:ge}
	\end{subfigure}
	
	\caption{Curve caratteristiche I-V del transistor; fit lineare nella regione attiva per \(\mathrm{\left|V_{CE}\right| \geq 1V}\)}
	\label{fig:car}
\end{figure}
Nella regione attiva (\(|\mathrm{V_{CE}| \gtrsim 1V}\)), la corrente di collettore ha un andamento lineare, si è quindi adattato il modello:
\begin{align}
&\mathrm{\left|I_C\right| = a + g\left|V_{CE}\right|}
\end{align}
dal risultato del fit(\autoref{fig:car}) sono state ricavate le stime della tensione di Early, della resistenza e della conduttanza di uscita del transistor. La tensione di Early è stata trovata imponendo:
\[\mathrm{I_C=0} \implies \mathrm{V_A} = \frac{a}{g}\]
mentre la resistenza b è data dall'inverso della conduttanza g, i valori ottenuti sono riportati in \autoref{tab:fit}.
\begin{table}[H]
	\centering
	\begin{tabular}{|l|c|c|}
			\cline{2-3}
		\multicolumn{1}{c|}{} & \multicolumn{1}{c|}{\(\mathbf{I_B = -200}\mathrm{\mu A}\)} & \multicolumn{1}{c|}{\(\mathbf{I_B = -100}\mathrm{\mu A}\)}\\\hline
		
		
		\(\mathrm{a(mA)}\) & $30.70\pm0.11$ &\(17.86\pm0.06\)\\\hline
		
		\(\mathrm{g}\left(\mathrm{mS}\right)\) & \(2.70\pm0.05\)&\(1.15\pm0.03\)\\\hline
		\(\mathrm{b(k\Omega)}\) & \(0.370\pm0.007\)&\(0.867\pm0.019\) \\\hline
		\(\mathrm{V_{A}(V)}\) & \(11.4\pm0.3\) & \(15.5\pm0.4\)\\\hline
	\end{tabular}
\caption{Grandezze ottenute dall'adattamento delle rette}
\label{tab:fit}
\end{table}
Infine si è stimato il guadagno di corrente dato dall'espressione:
	\[\mathrm{\beta = \frac{\Delta I_C}{\Delta I_B} = \frac{\left|I^{200\mu A}_{C}(V^*_{CE})-I^{100\mu A}_{C}(V^*_{CE})\right|}{100\mu A}}\]
prendendo in considerazione le due misure di \(\mathrm{I_C}\) fatte per \(\mathrm{\left|V_{CE}^*\right| = 3.1V}\), si è ottenuto quindi il valore \(\beta = 176\pm12\).
	

\section{Conclusioni}
L'adattamento lineare ha portato alla stima delle grandezze riportate in \autoref{tab:fit}; la differenza di potenziale tra collettore ed emettitore scelta porta a una stima del fattore di guadagno di corrente \(\beta=176\pm12\).
Nella regione attiva, il test del \(\chi^2\) non ha rilevato discrepanze tra le curve adattate e i dati sperimentali; si hanno valori di \(P(\chi^2>\chi^2_\nu)\approx 1\), questo suggerisce una sovrastima delle incertezze sulle misure, quindi sui parametri\cite{Minos} e sulle grandezze derivate. Un contributo alla sovrastima delle incertezze potrebbe essere dovuto alla scelta della risoluzione di lettura dell'oscilloscopio, che è arbitraria e dipende sia dallo spessore della linea visualizzata sia dalle capacità visive di chi misura. In conclusione il comportamento reale del transistor non si discosta in modo apprezzabile da quello atteso.
\appendix
\section{Stima delle Incertezze}
\subsection{Multimetro ISO TECH IDM 105}
Le incertezze sulle misure di tensione e corrente (in regime di corrente continua) sono state stimate secondo quanto riportato nelle specifiche del costruttore\cite{105} e sono riportate in \autoref{tab:emm}:
\begin{table}[H]
	\centering
	\begin{tabular}{|rl|rl|}
		\hline
		\multicolumn{2}{|c|}{\textbf{DCV}} & \multicolumn{2}{c|}{\textbf{DCA}}\\
		
		Fondo Scala & Incertezza(\(\pm\)) & Fondo Scala & Incertezza(\(\pm\)) \\
		\hline
		\hline
		400mV & \(0.3\% + 0.2\mathrm{mV}\) & 4mA & \(0.4\% + 2\mu\mathrm{A}\)\\
		\hline
		4V & \(0.1\% + 2\mathrm{mV}\) & 40mA & \(0.4\% + 20\mu\mathrm{A}\) \\
		\hline
		\multicolumn{2}{c|}{}& 400mA & \(0.4\% + 200\mu\mathrm{A}\) \\\cline{3-4}
		
	\end{tabular}
	\caption{Stime delle incertezze e relativo fondo scala}
\label{tab:emm}
\end{table}
\subsection{Oscilloscopio ISO TECH ISR622}
Le incertezze sulle misure di tensione ottenute con l'oscilloscopio sono state stimate sommando in quadratura gli errori di lettura e la sensibilità dello strumento (\(\sim\mathrm{1mV}\))\cite{osc} riportata dal costruttore:
\begin{equation}
	\delta\mathrm{V} = \pm\sqrt{\left(\frac{\mathrm{V/Div}_\mathrm{mis}}{20}\right)^2 + 
	\left(\frac{\mathrm{V/Div}_\mathrm{GND}}{20}\right)^2 + \left(\mathrm{1mV}\right)^2	
}
\end{equation}
Dove \(\mathrm{V/Div_{mis}}\) e \(\mathrm{V/Div_{GND}}\) sono i valori di Volt per divisione utilizzati rispettivamente per ottenere ciascuna misura e fissare il riferimento dell'oscilloscopio, quest'ultimo è stato fissato con la scala 5mV/Div. Come risoluzione si è scelto di utilizzare 1/4 di sottodivisione, ovvero \(\mathrm{\frac{V/Div}{20}}\).
\subsection{Parametri Fit}
I parametri dei fit sono stati stimati tramite minimizzazione numerica di \[\chi^2 =\sum_{i}{ \frac{\left[y_i-f(x_i;\vec{\alpha})\right]^2}{\sigma_{y_i}^2 + \frac{1}{4}\left[f(x_i+\sigma_{x_i};\vec{\alpha})-f(x_i-\sigma_{x_i};\vec{\alpha})\right]^2}}\] eseguita da ROOT; le incertezze associate alle stime dei parametri in \autoref{fig:car} sono state stimate come segue\cite{NumRec}: 
\begin{equation}
	\mathrm{\delta \alpha_i = \pm\sqrt{\Delta\chi^2_\nu H_{ii}^{-1}}}
\end{equation}
dove \(\mathrm{H_{ij} = \frac{\partial^2\chi^2}{\partial \alpha_i \partial \alpha_j}}\), \(\mathrm{\alpha_i}\) è il vettore dei parametri e \(\Delta\chi^2_\nu\) è la variazione di \(\chi^2_\nu\) associata ad un livello di confidenza del 68\%(\(1\sigma\)). %Gli intervalli di confidenza dei parametri riportati in \autoref{tab:fitpar} sono stati stimati da ROOT tramite il metodo MINOS ERRORS e in generale non sono simmetrici rispetto la stima del parametro\cite{Minos}.
\subsection{b}
La resistenza b è data dall'inverso della conduttanza g, stimata dal fit, abbiamo quindi:
\begin{equation}
	\mathrm{\delta b = \frac{\delta g}{g^2}}
\end{equation}
\subsection{\(\mathrm{V_A}\)}
La tensione di Early è data dal rapporto dei due parametri del fit:
\begin{equation}
\mathrm{V_A = \frac{a}{g}\implies \delta V_A = \left(\frac{\delta a}{a} + \frac{\delta g}{g}\right)\frac{a}{g}}
\end{equation}
\subsection{\(\beta\)}
Il guadagno di corrente è dato dal rapporto delle variazioni di corrente:
\begin{align*}
&\mathrm{\beta = \frac{\Delta I_C}{\Delta I_B} = \frac{\left|I^{200\mu A}_{C}(V^*_{CE})-I^{100\mu A}_{C}(V^*_{CE})\right|}{100\mu A}}
\end{align*}
si ottiene quindi:
\begin{align}
	&\mathrm{\delta\beta = \left[\frac{\delta I_{200\mu A} + \delta I_{100\mu A}}{100\mu A}+\frac{\delta I^{200\mu A}_{C}(V^*_{CE})+\delta I^{100\mu A}_{C}(V^*_{CE})}{\left| I^{200\mu A}_{C}(V^*_{CE})-I^{100\mu A}_{C}(V^*_{CE})\right|}\right]\beta}
\end{align}
\bibliography{refs.bib}{}
\bibliographystyle{siam}





\end{document}
