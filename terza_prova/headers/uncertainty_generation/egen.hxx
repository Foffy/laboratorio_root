/**
 * @file egen.hxx
 * @author Federico Ciampiconi (federico.ciampiconi@studio.unibo.it)
 * @brief 
 * @version 0.1
 * @date 2021-12-11
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#ifndef EGEN_HXX
#define EGEN_HXX
/**
 * @brief Generate Voltage Uncertainty for Oscilloscope
 * 
 * @param v -> Voltage Reading
 * @param vdiv -> V/Div of Reading
 * @return double -> Uncertainty Estimate
 */
double feV(double v, double vdiv);
/**
 * @brief Generate Current Uncertainty for MM105
 * 
 * @param v -> Current Reading
 * @param max -> Full-Scale
 * @return double -> Uncertainty Estimate
 */
double feI(double v, double max);
/**
 * @brief Generate Current Uncertainty for MM105
 * 
 * @param v -> Current Reading
 * @param max -> Full-Scale
 * @return double -> Uncertainty Estimate
 */
double femV(double v, double max);
#endif