#include "TFile.h"
#include "TCanvas.h"
#include "TH1.h"
#include "TH2.h"
#include "TF2.h"
#include "TObject.h"
#include "TStyle.h"
#include "TPaveStats.h"
#include "TTree.h"
#include "TSystem.h"
#include <iostream>
#include <fstream>
#include <iomanip>
#include <cmath>

int main()
{
      ////////////////////////////
      //      Setting Style     //
      ////////////////////////////

      gStyle->SetOptTitle(1);
      gStyle->SetOptStat(10);
      gStyle->SetOptFit(1110);
      gStyle->SetStatY(0.9);
      gStyle->SetStatX(0.9);
      gStyle->SetStatW(0.18);
      gStyle->SetStatH(0.18);

      Double_t w = 4000;
      Double_t h = 4000;

      //////////////////////////////////////////////
      //           BUILDING HISTOGRAMS            //
      //////////////////////////////////////////////

      gSystem->mkdir("OUTPUT"); //creating output folder

      TCanvas *canvas = new TCanvas();
      TH1::AddDirectory(kFALSE);

      TFile *gen_update = new TFile("Processed_Histos.root", "RECREATE");
      TFile *gen = new TFile("ANGLES.root", "READ");

      gen->ls();

      canvas->Divide(2, 1);
      canvas->cd(2);
      canvas->SetWindowSize(w, h / 4.);

      //TTree containing (theta, phi)
      TTree *angles = (TTree *)gen->Get("angles");

      int const entries = angles->GetEntries();
      float phi, theta;

      angles->SetBranchAddress("phi", &phi);
      angles->SetBranchAddress("theta", &theta);

      //Phi Distribution
      TH1F h_phi("phi", "#phi", 15, 0, 2 * M_PI);
      h_phi.SetXTitle("#phi (Radians)");
      h_phi.SetFillColor(17);
      h_phi.SetMarkerStyle(4);
      h_phi.SetMarkerColor(1);
      h_phi.SetLineWidth(4);
      h_phi.SetLineColor(1);

      //Theta Distribution
      TH1F h_theta("theta", "#theta", 15, 0, M_PI);
      h_theta.SetXTitle("#theta (Radians)");
      h_theta.SetFillColor(17);
      h_theta.SetMarkerStyle(4);
      h_theta.SetMarkerColor(1);
      h_theta.SetLineWidth(4);
      h_theta.SetLineColor(1);

      //Theta:Phi Distribution
      TH2F h_phi_theta("Theta_Phi", "(#theta, #phi) distribution", 20, 0, 2 * M_PI, 20, 0, M_PI);

      //Get (theta, phi) pairs from TTree and fill histograms
      for (int i = 0; i != entries; ++i)
      {
            angles->GetEntry(i);
            h_phi.Fill(phi);
            h_theta.Fill(theta);
            h_phi_theta.Fill(phi, theta);
      }
      h_phi.Fit("pol0", "quiet");
      h_phi.Draw("E,same");
      canvas->cd(1);
      h_theta.Fit("pol0", "quiet");
      h_theta.Draw("E,same");
      canvas->Print("OUTPUT/Angles.png");

      delete canvas;
      canvas = new TCanvas();
      canvas->SetWindowSize(2 * w, w);
      canvas->Divide(2, 1);
      canvas->cd(1);
      gStyle->SetHistMinimumZero(true);
      TF2 uniform("uniform", "[0]", 0, 2 * M_PI, 0, M_PI);
      h_phi_theta.SetLineColor(kGray);
      h_phi_theta.SetLineWidth(4);

      h_phi_theta.GetXaxis()->SetTitle("#phi (radians)");
      h_phi_theta.GetXaxis()->SetTitleOffset(2);
      h_phi_theta.GetYaxis()->SetTitle("#theta (radians)");
      h_phi_theta.GetYaxis()->SetTitleOffset(2);
      h_phi_theta.Fit(&uniform, "0,quiet");
      h_phi_theta.Draw("LEGO4");

      canvas->cd(2);

      uniform.SetTitle("(#theta, #phi) fitted distribution");
      uniform.GetXaxis()->SetTitle("#phi (radians)");
      uniform.GetXaxis()->SetTitleOffset(2);
      uniform.GetYaxis()->SetTitle("#theta (radians)");
      uniform.GetYaxis()->SetTitleOffset(2);
      uniform.Draw("surf");

      canvas->Print("OUTPUT/phitheta_corr.png");
      gen->Close();

      //////////////////////////////////////////////
      //     CREATING SUBTRACTION HISTOGRAMS      //
      //////////////////////////////////////////////

      gen = new TFile("file.root", "READ");
      gen->ls();
      delete canvas;
      canvas = new TCanvas();
      canvas->SetWindowSize(w, h / 4.);
      canvas->Divide(2, 1);
      TH1F *h1 = (TH1F *)gen->Get("k_p_d_imass;1");
      TH1F *h2 = (TH1F *)gen->Get("k_p_c_imass;1");
      h1->Sumw2(); //propagate uncertainty
      h2->Sumw2();

      TH1F subtraction3_4(*h1 - *h2);
      subtraction3_4.SetTitle("Kaon-Pion discordant I.M. - Kaon-Pion concordant I.M.");

      canvas->cd(1);
      subtraction3_4.SetEntries(subtraction3_4.Integral());
      subtraction3_4.GetXaxis()->SetTitleOffset(1.3);
      subtraction3_4.GetXaxis()->SetTitle("Invariant Mass #left(GeV #times c^{-2}#right)");
      subtraction3_4.SetFillColor(17);
      subtraction3_4.SetMarkerStyle(4);
      subtraction3_4.SetMarkerColor(1);
      subtraction3_4.SetLineWidth(4);
      subtraction3_4.SetLineColor(1);
      subtraction3_4.SetAxisRange(0.4, 1.4);
      subtraction3_4.Fit("gaus", "quiet");
      subtraction3_4.Draw("E");

      gen_update->WriteTObject(&subtraction3_4);

      h1 = (TH1F *)gen->Get("h_imassd;1");
      h2 = (TH1F *)gen->Get("h_imassc;1");
      h1->Sumw2(); //propagate uncertainty
      h2->Sumw2();

      canvas->cd(2);

      TH1F subtraction1_2(*h1 - *h2);
      subtraction1_2.SetTitle("discordant I.M. - concordant I.M.");

      subtraction1_2.SetEntries(subtraction1_2.Integral());
      subtraction1_2.GetXaxis()->SetTitleOffset(1.3);
      subtraction1_2.GetXaxis()->SetTitle("Invariant Mass #left(GeV #times c^{-2}#right)");
      subtraction1_2.SetFillColor(17);
      subtraction1_2.SetMarkerStyle(4);
      subtraction1_2.SetMarkerColor(1);
      subtraction1_2.SetLineWidth(4);
      subtraction1_2.SetLineColor(1);
      subtraction1_2.SetAxisRange(0.4, 1.4);
      subtraction1_2.Fit("gaus", "quiet");
      subtraction1_2.Draw("E");

      gen_update->WriteTObject(&subtraction1_2);

      canvas->Print("OUTPUT/Subtractions.png");

      //////////////////////////////////////////////
      //        CREATING OTHER HISTOGRAMS         //
      //////////////////////////////////////////////

      TH1F children_imass(*(TH1F *)gen->Get("children_imass;1")); //only children invariant mass distribution
      delete canvas;
      canvas = new TCanvas();
      canvas->Divide(1, 1);
      canvas->SetWindowSize(w, h);
      canvas->cd(1);
      children_imass.SetTitle("Resonance Children I.M.");

      children_imass.GetXaxis()->SetTitleOffset(1.3);
      children_imass.GetXaxis()->SetTitle("Invariant Mass #left(GeV #times c^{-2}#right)");
      children_imass.SetFillColor(17);
      children_imass.SetMarkerStyle(4);
      children_imass.SetMarkerColor(1);
      children_imass.SetLineWidth(4);
      children_imass.SetLineColor(1);
      children_imass.SetAxisRange(0.4, 1.4);
      children_imass.SetFillColor(kGray);
      children_imass.Fit("gaus", "quiet");
      children_imass.Draw("E,same");

      gen_update->WriteTObject(&children_imass);
      gen_update->Close();
      canvas->Print("OUTPUT/Children_Imass.png");

      /////////////////////////////////////////////////
      //  INVARIANT MASS DISTRIBUTIONS UNIQUE PRINT  //
      /////////////////////////////////////////////////

      delete canvas;
      canvas = new TCanvas();
      canvas->SetWindowSize(3 * w, h);
      canvas->Divide(3);
      canvas->cd(1);
      children_imass.SetFillStyle(1001);
      children_imass.SetFillColor(kGray);
      children_imass.Draw("same,E");
      canvas->cd(2);
      subtraction1_2.Draw();
      canvas->cd(3);
      subtraction3_4.Draw();
      canvas->Print("OUTPUT/imasses.png"); //print canvas with all invariant mass distributions
      gStyle->SetStatW(0.1);
      gStyle->SetStatH(0.1);

      delete canvas;
      canvas = new TCanvas();
      canvas->SetWindowSize(w, h / 2.);
      canvas->Divide(1, 1);
      canvas->cd(1);

      TH1F h_impulse(*(TH1F *)gen->Get("Impulse")); //impulse distribution
      h_impulse.SetXTitle("Impulse (GeV #times c^{-1})");
      h_impulse.SetFillColor(17);
      h_impulse.SetMarkerStyle(0);
      h_impulse.SetMarkerColor(1);
      h_impulse.SetLineWidth(4);
      h_impulse.SetLineColor(1);
      h_impulse.Fit("expo", "quiet");
      h_impulse.Draw("E,same");

      canvas->Print("OUTPUT/Impulse.png");

      //////////////////////////////////////////////
      //           PARTICLE PROPORTIONS           //
      //////////////////////////////////////////////

      delete canvas;
      canvas = new TCanvas();
      canvas->SetWindowSize(w, w);
      canvas->Divide(1, 1);
      TH1F h_ptypes(*(TH1F *)gen->Get("particle_types")); //particle types bar graph
      gen->Close();
      h_ptypes.SetTitle("Particle Types");
      h_ptypes.Draw();
      canvas->Print("OUTPUT/ptypes.png");
      delete canvas;
      float ppions = h_ptypes.GetBinContent(1) / entries;
      float npions = h_ptypes.GetBinContent(2) / entries;
      float pkaons = h_ptypes.GetBinContent(3) / entries;
      float nkaons = h_ptypes.GetBinContent(4) / entries;
      float pprotons = h_ptypes.GetBinContent(5) / entries;
      float nprotons = h_ptypes.GetBinContent(6) / entries;
      float Ks = h_ptypes.GetBinContent(7) / entries;
      float d_ppions = sqrt(ppions * entries * (1 - ppions)) / entries;
      float d_npions = sqrt(npions * entries * (1 - npions)) / entries;
      float d_pkaons = sqrt(pkaons * entries * (1 - pkaons)) / entries;
      float d_nkaons = sqrt(nkaons * entries * (1 - nkaons)) / entries;
      float d_pprotons = sqrt(pprotons * entries * (1 - pprotons)) / entries;
      float d_nprotons = sqrt(nprotons * entries * (1 - nprotons)) / entries;
      float d_Ks = sqrt(Ks * entries * (1 - Ks)) / entries;
      int N_children = children_imass.GetEntries() * 2;
      float dN_children = sqrt(double(N_children * (1 - (N_children / double(entries)))));

      gStyle->SetHistMinimumZero(false);

      //////////////////////////////////////////////
      //       Creating Canvas Divided in 4       //
      //////////////////////////////////////////////

      canvas = new TCanvas();
      canvas->Divide(2, 2);
      canvas->SetWindowSize(w, w);
      canvas->cd(1);
      h_ptypes.Draw();
      canvas->cd(2);
      h_impulse.Draw();
      canvas->cd(3);
      h_theta.Draw("E,same");
      canvas->cd(4);
      h_phi.Draw("E, same");
      canvas->Print("OUTPUT/four.png");
      delete canvas;

      //////////////////////////////////////////////
      //      MASS AND RWIDTH WEIGHTED AVGS       //
      //////////////////////////////////////////////

      double avg_imass = ((1 / (subtraction1_2.GetFunction("gaus")->GetParError(1) *
                                subtraction1_2.GetFunction("gaus")->GetParError(1))) *
                              subtraction1_2.GetFunction("gaus")->GetParameter(1) +
                          (1 / (subtraction3_4.GetFunction("gaus")->GetParError(1) *
                                subtraction3_4.GetFunction("gaus")->GetParError(1))) *
                              subtraction3_4.GetFunction("gaus")->GetParameter(1) +
                          (1 / (children_imass.GetFunction("gaus")->GetParError(1) *
                                children_imass.GetFunction("gaus")->GetParError(1))) *
                              children_imass.GetFunction("gaus")->GetParameter(1)) /
                         ((1 / (subtraction1_2.GetFunction("gaus")->GetParError(1) *
                                subtraction1_2.GetFunction("gaus")->GetParError(1))) +
                          (1 / (subtraction3_4.GetFunction("gaus")->GetParError(1) *
                                subtraction3_4.GetFunction("gaus")->GetParError(1))) +
                          (1 / (children_imass.GetFunction("gaus")->GetParError(1) *
                                children_imass.GetFunction("gaus")->GetParError(1))));
      double avg_imass_sigma = 1 / sqrt((1 / (subtraction1_2.GetFunction("gaus")->GetParError(1) *
                                              subtraction1_2.GetFunction("gaus")->GetParError(1))) +
                                        (1 / (subtraction3_4.GetFunction("gaus")->GetParError(1) *
                                              subtraction3_4.GetFunction("gaus")->GetParError(1))) +
                                        (1 / (children_imass.GetFunction("gaus")->GetParError(1) *
                                              children_imass.GetFunction("gaus")->GetParError(1))));
      double avg_sigma = ((1 / (subtraction1_2.GetFunction("gaus")->GetParError(2) *
                                subtraction1_2.GetFunction("gaus")->GetParError(2))) *
                              subtraction1_2.GetFunction("gaus")->GetParameter(2) +
                          (1 / (subtraction3_4.GetFunction("gaus")->GetParError(2) *
                                subtraction3_4.GetFunction("gaus")->GetParError(2))) *
                              subtraction3_4.GetFunction("gaus")->GetParameter(2) +
                          (1 / (children_imass.GetFunction("gaus")->GetParError(2) *
                                children_imass.GetFunction("gaus")->GetParError(2))) *
                              children_imass.GetFunction("gaus")->GetParameter(2)) /
                         ((1 / (subtraction1_2.GetFunction("gaus")->GetParError(2) *
                                subtraction1_2.GetFunction("gaus")->GetParError(2))) +
                          (1 / (subtraction3_4.GetFunction("gaus")->GetParError(2) *
                                subtraction3_4.GetFunction("gaus")->GetParError(2))) +
                          (1 / (children_imass.GetFunction("gaus")->GetParError(2) *
                                children_imass.GetFunction("gaus")->GetParError(2))));
      double avg_sigma_sigma = 1 / sqrt((1 / (subtraction1_2.GetFunction("gaus")->GetParError(2) *
                                              subtraction1_2.GetFunction("gaus")->GetParError(2))) +
                                        (1 / (subtraction3_4.GetFunction("gaus")->GetParError(2) *
                                              subtraction3_4.GetFunction("gaus")->GetParError(2))) +
                                        (1 / (children_imass.GetFunction("gaus")->GetParError(2) *
                                              children_imass.GetFunction("gaus")->GetParError(2))));

      //////////////////////////////////////////////
      //          DISCREPANCY EVALUATION          //
      //////////////////////////////////////////////
      // K* mass = 0.89166
      // K* r_width = 0.05
      // slope = -1

      TF1 standard_normal("standard", "gaus(0)");
      standard_normal.SetParameter(0, 1 / (sqrt(2 * M_PI)));
      standard_normal.SetParameter(1, 0);
      standard_normal.SetParameter(2, 1);

      //kaon-pion distr
      double z_o = abs((subtraction3_4.GetFunction("gaus")->GetParameter(1) - 0.89166) / (subtraction3_4.GetFunction("gaus")->GetParError(1)));
      double p_kp_mass = 1 - 2 * standard_normal.Integral(0, z_o);
      z_o = abs((subtraction3_4.GetFunction("gaus")->GetParameter(2) - 0.05) / (subtraction3_4.GetFunction("gaus")->GetParError(2)));
      double p_kp_rwidth = 1 - 2 * standard_normal.Integral(0, z_o);

      //allparticles
      z_o = abs((subtraction1_2.GetFunction("gaus")->GetParameter(1) - 0.89166) / (subtraction1_2.GetFunction("gaus")->GetParError(1)));
      double p_cd_mass = 1 - 2 * standard_normal.Integral(0, z_o);
      z_o = abs((subtraction1_2.GetFunction("gaus")->GetParameter(2) - 0.05) / (subtraction1_2.GetFunction("gaus")->GetParError(2)));
      double p_cd_rwidth = 1 - 2 * standard_normal.Integral(0, z_o);

      //children
      z_o = abs((children_imass.GetFunction("gaus")->GetParameter(1) - 0.89166) / (children_imass.GetFunction("gaus")->GetParError(1)));
      double p_ch_mass = 1 - 2 * standard_normal.Integral(0, z_o);
      z_o = abs((children_imass.GetFunction("gaus")->GetParameter(2) - 0.05) / (children_imass.GetFunction("gaus")->GetParError(2)));
      double p_ch_rwidth = 1 - 2 * standard_normal.Integral(0, z_o);

      //avg
      z_o = abs((avg_imass - 0.89166) / avg_imass_sigma);
      double p_avg_mass = 1 - 2 * standard_normal.Integral(0, z_o);
      z_o = abs((avg_sigma - 0.05) / avg_sigma_sigma);
      double p_avg_rwidth = 1 - 2 * standard_normal.Integral(0, z_o);

      //impulse
      //tau
      double tau = -1 / h_impulse.GetFunction("expo")->GetParameter(1);
      double d_tau = tau * tau * h_impulse.GetFunction("expo")->GetParError(1);
      z_o = abs((tau - 1) / (d_tau));
      double p_tau = 1 - 2 * standard_normal.Integral(0, z_o);

      //PARTICLE PROPORTIONS
      //pion+/- 0.4
      //kaon+/- 0.05
      //proton+/- 0.045
      //K* 0.01
      //children 0.01 * pentries * 2

      //////////////////////////////////////////////
      //      OUTPUTTING STATISTICAL RESULTS      //
      //////////////////////////////////////////////

      std::cout << std::fixed << '\n';
      std::cout << "********************STATISTICAL RESULTS********************\n"
                << '\n'
                << "PARTICLE TYPES DISTRIBUTION" << '\n'
                << "___________________________________________________________\n"
                << '\n'
                << "Particle Entries:                           " << entries + N_children << '\n'
                << "Number of Children:                         " << N_children << " ± " << dN_children << '\n'
                << "Particle Entries (K* children excluded):    " << entries << '\n'
                << "Particles Fraction:  " << '\n'
                << '\n'
                << "                  -----------------------------------------" << '\n'
                << "                  |    Pion(+):    " << ppions << " ± " << d_ppions << "    |" << '\n'
                << "                  -----------------------------------------" << '\n'
                << "                  |    Pion(-):    " << npions << " ± " << d_npions << "    |" << '\n'
                << "                  -----------------------------------------" << '\n'
                << "                  |    Kaon(+):    " << pkaons << " ± " << d_pkaons << "    |" << '\n'
                << "                  -----------------------------------------" << '\n'
                << "                  |    Kaon(-):    " << nkaons << " ± " << d_nkaons << "    |" << '\n'
                << "                  -----------------------------------------" << '\n'
                << "                  |    Proton(+):  " << pprotons << " ± " << d_pprotons << "    |" << '\n'
                << "                  -----------------------------------------" << '\n'
                << "                  |    Proton(-):  " << nprotons << " ± " << d_nprotons << "    |" << '\n'
                << "                  -----------------------------------------" << '\n'
                << "                  |    K*:         " << Ks << " ± " << d_Ks << "    |" << '\n'
                << "                  -----------------------------------------" << '\n'
                << "___________________________________________________________\n"
                << '\n'
                << "EXTRAPOLATED K* Mass & Resonance Width" << '\n'
                << "___________________________________________________________\n"
                << '\n'
                << "DISCORDANT CHARGE I.M. - CONCORDANT CHARGE I.M.\n"
                << '\n'
                << "K* mass:                     " << subtraction1_2.GetFunction("gaus")->GetParameter(1) << " ± " << subtraction1_2.GetFunction("gaus")->GetParError(1) << '\n'
                << "K* resonance width:          " << subtraction1_2.GetFunction("gaus")->GetParameter(2) << " ± " << subtraction1_2.GetFunction("gaus")->GetParError(2) << '\n'
                << "Gaussian Fit Normalization:  " << subtraction1_2.GetFunction("gaus")->GetParameter(0) << " ± " << subtraction1_2.GetFunction("gaus")->GetParError(0) << '\n'
                << "X²obs / nDF Gaussian Fit:    " << subtraction1_2.GetFunction("gaus")->GetChisquare() << " / " << subtraction1_2.GetFunction("gaus")->GetNDF() << '\n'
                << "P(X² > X²obs):               " << subtraction1_2.GetFunction("gaus")->GetProb() << "    "
                << (subtraction1_2.GetFunction("gaus")->GetProb() < 0.05 ? "There is EVIDENCE of fit discrepancy" : "There is NO EVIDENCE of fit discrepancy") << '\n'
                << "P(z_µ > z_µobs):             " << p_cd_mass << "    "
                << (p_cd_mass < 0.05 ? "There is EVIDENCE of discrepancy" : "There is NO EVIDENCE of discrepancy") << '\n'
                << "P(z_σ > z_σobs):             " << p_cd_rwidth << "    "
                << (p_cd_rwidth < 0.05 ? "There is EVIDENCE of discrepancy" : "There is NO EVIDENCE of discrepancy") << '\n'
                << '\n'
                << "KAON-PION DISCORDANT I.M. - CONCORDANT I.M.\n"
                << '\n'
                << "K* mass:                     " << subtraction3_4.GetFunction("gaus")->GetParameter(1) << " ± " << subtraction3_4.GetFunction("gaus")->GetParError(1) << '\n'
                << "K* resonance width:          " << subtraction3_4.GetFunction("gaus")->GetParameter(2) << " ± " << subtraction3_4.GetFunction("gaus")->GetParError(2) << '\n'
                << "Gaussian Fit Normalization:  " << subtraction3_4.GetFunction("gaus")->GetParameter(0) << " ± " << subtraction3_4.GetFunction("gaus")->GetParError(0) << '\n'
                << "X²obs / nDF Gaussian Fit:    " << subtraction3_4.GetFunction("gaus")->GetChisquare() << " / " << subtraction3_4.GetFunction("gaus")->GetNDF() << '\n'
                << "P(X² > X²obs):               " << subtraction3_4.GetFunction("gaus")->GetProb() << "    "
                << (subtraction3_4.GetFunction("gaus")->GetProb() < 0.05 ? "There is EVIDENCE of fit discrepancy" : "There is NO EVIDENCE of fit discrepancy") << '\n'
                << "P(z_µ > z_µobs):             " << p_kp_mass << "    "
                << (p_kp_mass < 0.05 ? "There is EVIDENCE of discrepancy" : "There is NO EVIDENCE of discrepancy") << '\n'
                << "P(z_σ > z_σobs):             " << p_kp_rwidth << "    "
                << (p_kp_rwidth < 0.05 ? "There is EVIDENCE of discrepancy" : "There is NO EVIDENCE of discrepancy") << '\n'
                << '\n'
                << "CHILDREN INVARIANT MASS\n"
                << '\n'
                << "K* mass:                     " << children_imass.GetFunction("gaus")->GetParameter(1) << " ± " << children_imass.GetFunction("gaus")->GetParError(1) << '\n'
                << "K* resonance width:          " << children_imass.GetFunction("gaus")->GetParameter(2) << " ± " << children_imass.GetFunction("gaus")->GetParError(2) << '\n'
                << "Gaussian Fit Normalization:  " << children_imass.GetFunction("gaus")->GetParameter(0) << " ± " << children_imass.GetFunction("gaus")->GetParError(0) << '\n'
                << "Gaussian Fit X²obs / nDF:    " << children_imass.GetFunction("gaus")->GetChisquare() << " / " << children_imass.GetFunction("gaus")->GetNDF() << '\n'
                << "P(X² > X²obs):               " << children_imass.GetFunction("gaus")->GetProb() << "    "
                << (children_imass.GetFunction("gaus")->GetProb() < 0.05 ? "There is EVIDENCE of fit discrepancy" : "There is NO EVIDENCE of fit discrepancy") << '\n'
                << "P(z_µ > z_µobs):             " << p_ch_mass << "    "
                << (p_ch_mass < 0.05 ? "There is EVIDENCE of discrepancy" : "There is NO EVIDENCE of discrepancy") << '\n'
                << "P(z_σ > z_σobs):             " << p_ch_rwidth << "    "
                << (p_ch_rwidth < 0.05 ? "There is EVIDENCE of discrepancy" : "There is NO EVIDENCE of discrepancy") << '\n'
                << '\n'
                << "WEIGHTED AVERAGE\n"
                << '\n'
                << "K* mass:             " << avg_imass << " ± " << avg_imass_sigma << '\n'
                << "K* resonance width:  " << avg_sigma << " ± " << avg_sigma_sigma << '\n'
                << "P(z_µ > z_µobs):     " << p_avg_mass << "    "
                << (p_avg_mass < 0.05 ? "There is EVIDENCE of discrepancy" : "There is NO EVIDENCE of discrepancy") << '\n'
                << "P(z_σ > z_σobs):     " << p_avg_rwidth << "    "
                << (p_avg_rwidth < 0.05 ? "There is EVIDENCE of discrepancy" : "There is NO EVIDENCE of discrepancy") << '\n'
                << "___________________________________________________________\n"
                << '\n'
                << "IMPULSE DISTRIBUTION" << '\n'
                << "___________________________________________________________\n"
                << '\n'
                << "Exponential Fit Normalization:  " << h_impulse.GetFunction("expo")->GetParameter(0) << " ± " << h_impulse.GetFunction("expo")->GetParError(0) << '\n'
                << "Exponential Fit Slope:          " << h_impulse.GetFunction("expo")->GetParameter(1) << " ± " << h_impulse.GetFunction("expo")->GetParError(1) << '\n'
                << "tau:                            " << tau << " ± " << d_tau << '\n'
                << "Exponential Fit X²obs / nDF:    " << h_impulse.GetFunction("expo")->GetChisquare() << " / " << h_impulse.GetFunction("expo")->GetNDF() << '\n'
                << "P(X² > X²obs):                  " << h_impulse.GetFunction("expo")->GetProb() << "    "
                << (h_impulse.GetFunction("expo")->GetProb() < 0.05 ? "There is EVIDENCE of fit discrepancy" : "There is NO EVIDENCE of fit discrepancy") << '\n'
                << "P(z_tau > z_tauobs):            " << p_tau << "    "
                << (p_tau < 0.05 ? "There is EVIDENCE of discrepancy" : "There is NO EVIDENCE of discrepancy") << '\n'
                << "___________________________________________________________\n"
                << '\n'
                << "PHI - THETA" << '\n'
                << "___________________________________________________________\n"
                << '\n'
                << "CORRELATION\n"
                << '\n'
                << "Phi-Theta Covariance:             " << h_phi_theta.GetCovariance() << '\n'
                << "Bivariate Uniform Normalization:  " << uniform.GetParameter(0) << " ± " << uniform.GetParError(0) << '\n'
                << "Bivariate Uniform X²obs / nDF:    " << uniform.GetChisquare() << " / " << uniform.GetNDF() << '\n'
                << "P(X² > X²obs):                    " << uniform.GetProb() << "    "
                << (uniform.GetProb() < 0.05 ? "There is EVIDENCE of fit discrepancy" : "There is NO EVIDENCE of fit discrepancy \n") << '\n'
                << "THETA" << '\n'
                << "Uniform Fit Normalization:  " << h_theta.GetFunction("pol0")->GetParameter(0) << " ± " << h_theta.GetFunction("pol0")->GetParError(0) << '\n'
                << "Uniform X²obs/nDF:          " << h_theta.GetFunction("pol0")->GetChisquare() << " / " << h_theta.GetFunction("pol0")->GetNDF() << '\n'
                << "P(X² > X²obs):              " << h_theta.GetFunction("pol0")->GetProb() << "    "
                << (h_theta.GetFunction("pol0")->GetProb() < 0.05 ? "There is EVIDENCE of fit discrepancy" : "There is NO EVIDENCE of fit discrepancy") << '\n'
                << "PHI" << '\n'
                << "Uniform Fit Normalization:  " << h_phi.GetFunction("pol0")->GetParameter(0) << " ± " << h_phi.GetFunction("pol0")->GetParError(0) << '\n'
                << "Uniform X²obs/nDF:          " << h_phi.GetFunction("pol0")->GetChisquare() << " / " << h_phi.GetFunction("pol0")->GetNDF() << '\n'
                << "P(X² > X²obs):              " << h_phi.GetFunction("pol0")->GetProb() << "    "
                << (h_phi.GetFunction("pol0")->GetProb() < 0.05 ? "There is EVIDENCE of fit discrepancy" : "There is NO EVIDENCE of fit discrepancy") << '\n'

                << "___________________________________________________________\n"
                << '\n'
                << "***********************************************************" << std::endl;
      std::ofstream stats_file("OUTPUT/results.txt");
      stats_file << std::fixed;
      stats_file << "********************STATISTICAL RESULTS********************\n"
                 << '\n'
                 << "PARTICLE TYPES DISTRIBUTION" << '\n'
                 << "___________________________________________________________\n"
                 << '\n'
                 << "Particle Entries:                           " << entries + N_children << '\n'
                 << "Number of Children:                         " << N_children << " ± " << dN_children << '\n'
                 << "Particle Entries (K* children excluded):    " << entries << '\n'
                 << "Particles Fraction:  " << '\n'
                 << '\n'
                 << "                  -----------------------------------------" << '\n'
                 << "                  |    Pion(+):    " << ppions << " ± " << d_ppions << "    |" << '\n'
                 << "                  -----------------------------------------" << '\n'
                 << "                  |    Pion(-):    " << npions << " ± " << d_npions << "    |" << '\n'
                 << "                  -----------------------------------------" << '\n'
                 << "                  |    Kaon(+):    " << pkaons << " ± " << d_pkaons << "    |" << '\n'
                 << "                  -----------------------------------------" << '\n'
                 << "                  |    Kaon(-):    " << nkaons << " ± " << d_nkaons << "    |" << '\n'
                 << "                  -----------------------------------------" << '\n'
                 << "                  |    Proton(+):  " << pprotons << " ± " << d_pprotons << "    |" << '\n'
                 << "                  -----------------------------------------" << '\n'
                 << "                  |    Proton(-):  " << nprotons << " ± " << d_nprotons << "    |" << '\n'
                 << "                  -----------------------------------------" << '\n'
                 << "                  |    K*:         " << Ks << " ± " << d_Ks << "    |" << '\n'
                 << "                  -----------------------------------------" << '\n'
                 << "___________________________________________________________\n"
                 << '\n'
                 << "EXTRAPOLATED K* Mass & Resonance Width" << '\n'
                 << "___________________________________________________________\n"
                 << '\n'
                 << "DISCORDANT CHARGE I.M. - CONCORDANT CHARGE I.M.\n"
                 << '\n'
                 << "K* mass:                     " << subtraction1_2.GetFunction("gaus")->GetParameter(1) << " ± " << subtraction1_2.GetFunction("gaus")->GetParError(1) << '\n'
                 << "K* resonance width:          " << subtraction1_2.GetFunction("gaus")->GetParameter(2) << " ± " << subtraction1_2.GetFunction("gaus")->GetParError(2) << '\n'
                 << "Gaussian Fit Normalization:  " << subtraction1_2.GetFunction("gaus")->GetParameter(0) << " ± " << subtraction1_2.GetFunction("gaus")->GetParError(0) << '\n'
                 << "X²obs / nDF Gaussian Fit:    " << subtraction1_2.GetFunction("gaus")->GetChisquare() << " / " << subtraction1_2.GetFunction("gaus")->GetNDF() << '\n'
                 << "P(X² > X²obs):               " << subtraction1_2.GetFunction("gaus")->GetProb() << "    "
                 << (subtraction1_2.GetFunction("gaus")->GetProb() < 0.05 ? "There is EVIDENCE of fit discrepancy" : "There is NO EVIDENCE of fit discrepancy") << '\n'
                 << "P(z_µ > z_µobs):             " << p_cd_mass << "    "
                 << (p_cd_mass < 0.05 ? "There is EVIDENCE of discrepancy" : "There is NO EVIDENCE of discrepancy") << '\n'
                 << "P(z_σ > z_σobs):             " << p_cd_rwidth << "    "
                 << (p_cd_rwidth < 0.05 ? "There is EVIDENCE of discrepancy" : "There is NO EVIDENCE of discrepancy") << '\n'
                 << '\n'
                 << "KAON-PION DISCORDANT I.M. - CONCORDANT I.M.\n"
                 << '\n'
                 << "K* mass:                     " << subtraction3_4.GetFunction("gaus")->GetParameter(1) << " ± " << subtraction3_4.GetFunction("gaus")->GetParError(1) << '\n'
                 << "K* resonance width:          " << subtraction3_4.GetFunction("gaus")->GetParameter(2) << " ± " << subtraction3_4.GetFunction("gaus")->GetParError(2) << '\n'
                 << "Gaussian Fit Normalization:  " << subtraction3_4.GetFunction("gaus")->GetParameter(0) << " ± " << subtraction3_4.GetFunction("gaus")->GetParError(0) << '\n'
                 << "X²obs / nDF Gaussian Fit:    " << subtraction3_4.GetFunction("gaus")->GetChisquare() << " / " << subtraction3_4.GetFunction("gaus")->GetNDF() << '\n'
                 << "P(X² > X²obs):               " << subtraction3_4.GetFunction("gaus")->GetProb() << "    "
                 << (subtraction3_4.GetFunction("gaus")->GetProb() < 0.05 ? "There is EVIDENCE of fit discrepancy" : "There is NO EVIDENCE of fit discrepancy") << '\n'
                 << "P(z_µ > z_µobs):             " << p_kp_mass << "    "
                 << (p_kp_mass < 0.05 ? "There is EVIDENCE of discrepancy" : "There is NO EVIDENCE of discrepancy") << '\n'
                 << "P(z_σ > z_σobs):             " << p_kp_rwidth << "    "
                 << (p_kp_rwidth < 0.05 ? "There is EVIDENCE of discrepancy" : "There is NO EVIDENCE of discrepancy") << '\n'
                 << '\n'
                 << "CHILDREN INVARIANT MASS\n"
                 << '\n'
                 << "K* mass:                     " << children_imass.GetFunction("gaus")->GetParameter(1) << " ± " << children_imass.GetFunction("gaus")->GetParError(1) << '\n'
                 << "K* resonance width:          " << children_imass.GetFunction("gaus")->GetParameter(2) << " ± " << children_imass.GetFunction("gaus")->GetParError(2) << '\n'
                 << "Gaussian Fit Normalization:  " << children_imass.GetFunction("gaus")->GetParameter(0) << " ± " << children_imass.GetFunction("gaus")->GetParError(0) << '\n'
                 << "Gaussian Fit X²obs / nDF:    " << children_imass.GetFunction("gaus")->GetChisquare() << " / " << children_imass.GetFunction("gaus")->GetNDF() << '\n'
                 << "P(X² > X²obs):               " << children_imass.GetFunction("gaus")->GetProb() << "    "
                 << (children_imass.GetFunction("gaus")->GetProb() < 0.05 ? "There is EVIDENCE of fit discrepancy" : "There is NO EVIDENCE of fit discrepancy") << '\n'
                 << "P(z_µ > z_µobs):             " << p_ch_mass << "    "
                 << (p_ch_mass < 0.05 ? "There is EVIDENCE of discrepancy" : "There is NO EVIDENCE of discrepancy") << '\n'
                 << "P(z_σ > z_σobs):             " << p_ch_rwidth << "    "
                 << (p_ch_rwidth < 0.05 ? "There is EVIDENCE of discrepancy" : "There is NO EVIDENCE of discrepancy") << '\n'
                 << '\n'
                 << "WEIGHTED AVERAGE\n"
                 << '\n'
                 << "K* mass:             " << avg_imass << " ± " << avg_imass_sigma << '\n'
                 << "K* resonance width:  " << avg_sigma << " ± " << avg_sigma_sigma << '\n'
                 << "P(z_µ > z_µobs):     " << p_avg_mass << "    "
                 << (p_avg_mass < 0.05 ? "There is EVIDENCE of discrepancy" : "There is NO EVIDENCE of discrepancy") << '\n'
                 << "P(z_σ > z_σobs):     " << p_avg_rwidth << "    "
                 << (p_avg_rwidth < 0.05 ? "There is EVIDENCE of discrepancy" : "There is NO EVIDENCE of discrepancy") << '\n'
                 << "___________________________________________________________\n"
                 << '\n'
                 << "IMPULSE DISTRIBUTION" << '\n'
                 << "___________________________________________________________\n"
                 << '\n'
                 << "Exponential Fit Normalization:  " << h_impulse.GetFunction("expo")->GetParameter(0) << " ± " << h_impulse.GetFunction("expo")->GetParError(0) << '\n'
                 << "Exponential Fit Slope:          " << h_impulse.GetFunction("expo")->GetParameter(1) << " ± " << h_impulse.GetFunction("expo")->GetParError(1) << '\n'
                 << "tau:                            " << tau << " ± " << d_tau << '\n'
                 << "Exponential Fit X²obs / nDF:    " << h_impulse.GetFunction("expo")->GetChisquare() << " / " << h_impulse.GetFunction("expo")->GetNDF() << '\n'
                 << "P(X² > X²obs):                  " << h_impulse.GetFunction("expo")->GetProb() << "    "
                 << (h_impulse.GetFunction("expo")->GetProb() < 0.05 ? "There is EVIDENCE of fit discrepancy" : "There is NO EVIDENCE of fit discrepancy") << '\n'
                 << "P(z_tau > z_tauobs):            " << p_tau << "    "
                 << (p_tau < 0.05 ? "There is EVIDENCE of discrepancy" : "There is NO EVIDENCE of discrepancy") << '\n'
                 << "___________________________________________________________\n"
                 << '\n'
                 << "PHI - THETA" << '\n'
                 << "___________________________________________________________\n"
                 << '\n'
                 << "CORRELATION\n"
                 << '\n'
                 << "Phi-Theta Covariance:             " << h_phi_theta.GetCovariance() << '\n'
                 << "Bivariate Uniform Normalization:  " << uniform.GetParameter(0) << " ± " << uniform.GetParError(0) << '\n'
                 << "Bivariate Uniform X²obs / nDF:    " << uniform.GetChisquare() << " / " << uniform.GetNDF() << '\n'
                 << "P(X² > X²obs):                    " << uniform.GetProb() << "    "
                 << (uniform.GetProb() < 0.05 ? "There is EVIDENCE of fit discrepancy" : "There is NO EVIDENCE of fit discrepancy \n") << '\n'
                 << "THETA" << '\n'
                 << "Uniform Fit Normalization:  " << h_theta.GetFunction("pol0")->GetParameter(0) << " ± " << h_theta.GetFunction("pol0")->GetParError(0) << '\n'
                 << "Uniform X²obs/nDF:          " << h_theta.GetFunction("pol0")->GetChisquare() << " / " << h_theta.GetFunction("pol0")->GetNDF() << '\n'
                 << "P(X² > X²obs):              " << h_theta.GetFunction("pol0")->GetProb() << "    "
                 << (h_theta.GetFunction("pol0")->GetProb() < 0.05 ? "There is EVIDENCE of fit discrepancy" : "There is NO EVIDENCE of fit discrepancy") << '\n'
                 << "PHI" << '\n'
                 << "Uniform Fit Normalization:  " << h_phi.GetFunction("pol0")->GetParameter(0) << " ± " << h_phi.GetFunction("pol0")->GetParError(0) << '\n'
                 << "Uniform X²obs/nDF:          " << h_phi.GetFunction("pol0")->GetChisquare() << " / " << h_phi.GetFunction("pol0")->GetNDF() << '\n'
                 << "P(X² > X²obs):              " << h_phi.GetFunction("pol0")->GetProb() << "    "
                 << (h_phi.GetFunction("pol0")->GetProb() < 0.05 ? "There is EVIDENCE of fit discrepancy" : "There is NO EVIDENCE of fit discrepancy") << '\n'

                 << "___________________________________________________________\n"
                 << '\n'
                 << "***********************************************************" << std::endl;
      gen->Close();
}
