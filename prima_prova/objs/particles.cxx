#include "particles.hpp"
#include <iostream>
#include <cmath>
#include <cstdlib>

//PARTICLE STATIC DATA MEMBERS

std::vector<particle_type const *> particle::particles_(0);
int particle::fNParticle_Types_(0);
int particle::Max_PTypes_(10);

//CONSTRUCTOR(s)
/**
 * @brief Construct a new particle::particle object
 * 
 * @param fName name of particle type
 * @param momentum <- momentum vector
 */
particle::particle(std::string const &fName, Vector3D<float> const &momentum = Vector3D<float>{}) : momentum_{momentum}
{
    Find_pType(fName) != -1 ? index_ = Find_pType(fName) : throw std::logic_error("No matching particle type");
}
/**
 * @brief Construct a new particle::particle object
 * 
 * @param index index of particle type pointer
 * @param momentum <- momentum vector
 */
particle::particle(int const index, Vector3D<float> const &momentum = Vector3D<float>{}) : momentum_{momentum}
{
    (index != -1 && index < int(particles_.size())) ? index_ = index : throw std::logic_error("No matching particle type");
}
/**
 * @brief Free heap memory allocated for particle_type objects
 * 
 */
void particle::delete_ptypes()
{
    for (int i = 0; i != fNParticle_Types_; ++i)
    {
        delete particles_[i];
        particles_[i] = nullptr;
    }
}

//PRINT METHODS

/**
 * @brief Print particle properties
 * 
 */
void particle::print_pProperties() const
{
    particles_[index_]->print();
    std::cout << "P = ";
    momentum_.print();
    std::cout << "Index in Array: " << index_ << '\n';
}

//GET METHODS

/**
 * @brief Find particle type by name
 * 
 * @param fName particle type name
 * @return int <- particle type address
 */
int particle::Find_pType(std::string const &fName)
{
    int index = -1;
    for (int i = 0; i != fNParticle_Types_;)
    {
        if (fName == particles_[i]->get_name())
            index = i;
        ++i;
    }
    return index;
}

/**
 * @brief Compute Invariant Mass
 * 
 * @param p2 Second particle
 * @return double <- Invariant mass
 */
double particle::get_iMass(particle const &p2) const
{
    return sqrt(
        (get_pEnergy() + p2.get_pEnergy()) *
            (get_pEnergy() + p2.get_pEnergy()) -
        (momentum_ + p2.momentum_) *
            (momentum_ + p2.momentum_));
}
/**
 * @brief Get particle energy
 * 
 * @return double <- Energy
 */
double particle::get_pEnergy() const
{
    return sqrt(
        particles_[index_]->get_mass() * particles_[index_]->get_mass() +
        momentum_.get_Norm() * momentum_.get_Norm());
}

//STATIC METHODS
/**
 * @brief Add a new particle type
 * 
 * @param fName name 
 * @param mass  rest mass 
 * @param charge charge 
 * @param resonance resonance width 
 */
void particle::add_particle_type(std::string const &fName, double const mass, int const charge, double const resonance)
{
    if (fNParticle_Types_ == Max_PTypes_)
    {
        throw std::runtime_error("Too many particle types");
    }
    for (auto const e : particles_)
    {
        if (e->get_name() == fName)
            return;
    }
    if (resonance == -1)
    {
        particle_type *ptr = new particle_type(fName, mass, charge);
        particles_.push_back(ptr);
    }
    else
    {
        particle_type *ptr = new resonance_type(fName, mass, charge, resonance);
        particles_.push_back(ptr);
    }

    ++fNParticle_Types_;
}

//SET METHODS

/**
 * @brief set the object to be associated with a specific particle type
 * 
 * @param fName particle type name
 */
void particle::set_pType(std::string const &fName)
{
    int index = Find_pType(fName);
    (index != -1) ? index_ = index : throw(std::logic_error("No matching particle type"));
}
/**
 * @brief set the object to be associated with a specific particle type
 * 
 * @param index index of particle type pointer
 */
void particle::set_pType(int const index)
{
    (index != -1 && index < int(particles_.size())) ? index_ = index : throw std::logic_error("No matching particle type");
}
/**
 * @brief Set the particle momentum
 * 
 * @param m momentum vector (Cartesian Coordinates)
 */
void particle::set_Momentum(Vector3D<float> const &m = Vector3D<float>())
{
    momentum_ = m;
}
/**
 * @brief Set the particle momentum (Spherical Coordinates)
 * 
 * @param rho norm
 * @param theta inclination
 * @param phi azimuth
 */
void particle::set_Momentum(float const rho, float const theta, float const phi)
{
    if (phi > 2 * M_PI || phi < 0 || theta > M_PI || theta < 0)
    {
        throw std::logic_error("Invalid Angle Value");
    }
    float x = rho * std::sin(theta) * std::sin(phi);
    float y = rho * std::sin(theta) * std::cos(phi);
    float z = rho * std::cos(theta);
    momentum_(x, y, z);
}
/**
 * @brief Decay *this particle into 2 particle objects
 * 
 * @param dau1 first particle
 * @param dau2 second particle
 * @return int - 0 success
 */
int particle::Decay2body(particle &dau1, particle &dau2) const
{
    if (get_pMass() == 0.0)
    {
        std::cout << "Decayment cannot be preformed if mass is zero\n";
        return 1;
    }

    double massMot = get_pMass();
    double massDau1 = dau1.get_pMass();
    double massDau2 = dau2.get_pMass();

    if (index_ > -1)
    { // add width effect

        // gaussian random numbers

        float x1, x2, w, y1;

        double invnum = 1. / RAND_MAX;
        do
        {
            x1 = 2.0 * rand() * invnum - 1.0;
            x2 = 2.0 * rand() * invnum - 1.0;
            w = x1 * x1 + x2 * x2;
        } while (w >= 1.0);

        w = sqrt((-2.0 * log(w)) / w);
        y1 = x1 * w;

        massMot += particles_[index_]->get_fWidth() * y1;
    }

    if (massMot < massDau1 + massDau2)
    {
        std::cout << "Decayment cannot be preformed because mass is too low in this channel\n";
        return 2;
    }

    double pout = sqrt((massMot * massMot - (massDau1 + massDau2) * (massDau1 + massDau2)) * (massMot * massMot - (massDau1 - massDau2) * (massDau1 - massDau2))) / massMot * 0.5;

    double norm = 2 * M_PI / RAND_MAX;

    double phi = rand() * norm;
    double theta = rand() * norm * 0.5 - M_PI / 2.;
    dau1.set_Momentum({float(pout * sin(theta) * cos(phi)), float(pout * sin(theta) * sin(phi)), float(pout * cos(theta))});
    dau2.set_Momentum({float(-pout * sin(theta) * cos(phi)), float(-pout * sin(theta) * sin(phi)), float(-pout * cos(theta))});

    double energy = sqrt(
        momentum_.x() * momentum_.x() +
        momentum_.y() * momentum_.y() +
        momentum_.z() * momentum_.z() +
        massMot * massMot);

    double bx = momentum_.x() / energy;
    double by = momentum_.y() / energy;
    double bz = momentum_.z() / energy;

    dau1.Boost(bx, by, bz);
    dau2.Boost(bx, by, bz);

    return 0;
}
/**
 * @brief Lorentz transformation
 * 
 * @param bx x component
 * @param by y component
 * @param bz z component
 */
void particle::Boost(double bx, double by, double bz)
{

    double energy = get_pEnergy();

    //Boost this Lorentz vector
    double b2 = bx * bx + by * by + bz * bz;
    double gamma = 1.0 / sqrt(1.0 - b2);
    double bp = bx * momentum_.x() + by * momentum_.y() + bz * momentum_.z();
    double gamma2 = b2 > 0 ? (gamma - 1.0) / b2 : 0.0;

    momentum_(
        momentum_.x() + gamma2 * bp * bx + gamma * bx * energy,
        momentum_.y() + gamma2 * bp * by + gamma * by * energy,
        momentum_.z() + gamma2 * bp * bz + gamma * bz * energy);
}
