#include <vector>
#include <random>
#include <cmath>
#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <chrono>
#include <ctime>
#include "TH1F.h"
#include "TFile.h"
#include "TTree.h"
#include "particles.hpp"
#include "event.hpp"
#include "coord_converter.hpp"

int main()
{
    particle::add_particle_type("pion+", 0.13957, +1);
    particle::add_particle_type("pion-", 0.13957, -1);
    particle::add_particle_type("kaon+", 0.49367, +1);
    particle::add_particle_type("kaon-", 0.49367, -1);
    particle::add_particle_type("proton+", 0.93827, +1);
    particle::add_particle_type("proton-", 0.93827, -1);
    particle::add_particle_type("K*", 0.89166, 0, 0.05);
    int const N = 200000;
    std::vector<event> events(N);

    double ppions = 0;
    double npions = 0;
    double pkaons = 0;
    double nkaons = 0;
    double pprotons = 0;
    double nprotons = 0;
    double K = 0;

    std::clock_t c_start = std::clock();
    auto t_start = std::chrono::high_resolution_clock::now();
    {
        TFile angles("ANGLES.root", "RECREATE");
        TTree tree("angles", "angles");
        float theta, phi;
        tree.Branch("theta", &theta);
        tree.Branch("phi", &phi);
        std::mt19937 gen(std::random_device{}());
        for (int i = 0; i != N; ++i)
        {
            event e;
            e.particles.reserve(110);

            std::uniform_real_distribution<float> particledistr(0, 1);
            std::uniform_real_distribution<float> phidistr(0, 2 * M_PI);
            std::uniform_real_distribution<float> thetadistr(0, M_PI);
            std::exponential_distribution<float> momentum(1);

            for (int i = 0; i != 100; ++i)
            {
                phi = phidistr(gen);
                theta = thetadistr(gen);
                tree.Fill();
                float random = particledistr(gen);
                float momentumnorm = momentum(gen);
                if (random < 0.8f)
                {
                    if (particledistr(gen) < 0.5f)
                    {
                        e.particles.push_back(particle(0, to_cartesian(momentumnorm, theta, phi))); //pion+
                        ++ppions;
                    }
                    else
                    {
                        e.particles.push_back(particle(1, to_cartesian(momentumnorm, theta, phi))); //pion-
                        ++npions;
                    }
                }
                else if (random < 0.9f)
                {
                    if (particledistr(gen) < 0.5f)
                    {
                        e.particles.push_back(particle(2, to_cartesian(momentumnorm, theta, phi))); //kaon+
                        ++pkaons;
                    }
                    else
                    {
                        e.particles.push_back(particle(3, to_cartesian(momentumnorm, theta, phi))); //kaon-
                        ++nkaons;
                    }
                }
                else if (random < 0.99f)
                {
                    if (particledistr(gen) < 0.5f)
                    {
                        e.particles.push_back(particle(4, to_cartesian(momentumnorm, theta, phi))); //proton+
                        ++pprotons;
                    }
                    else
                    {
                        e.particles.push_back(particle(5, to_cartesian(momentumnorm, theta, phi))); //proton-
                        ++nprotons;
                    }
                }
                else
                {
                    ++K;
                    particle K(6, to_cartesian(momentumnorm, theta, phi)); //K*
                    e.particles.push_back(K);
                    float theta_1 = thetadistr(gen);
                    float theta_2 = thetadistr(gen);
                    float phi_1 = thetadistr(gen);
                    float phi_2 = thetadistr(gen);
                    if (particledistr(gen) < 0.5f)
                    {
                        particle p1(0, to_cartesian(momentum(gen), theta_1, phi_1)); //pion+
                        particle p2(3, to_cartesian(momentum(gen), theta_2, phi_2)); //kaon-
                        K.Decay2body(p1, p2);
                        e.particles.push_back(p1);
                        e.particles.push_back(p2);
                    }
                    else
                    {
                        particle p1(1, to_cartesian(momentum(gen), theta_1, phi_1)); //pion-
                        particle p2(2, to_cartesian(momentum(gen), theta_2, phi_2)); //kaon+
                        K.Decay2body(p1, p2);
                        e.particles.push_back(p1);
                        e.particles.push_back(p2);
                    }
                }
            }
            events[i] = e;
        }
        tree.Write();
        angles.Close();
    }
    std::clock_t c_end = std::clock();
    auto t_end = std::chrono::high_resolution_clock::now();

    std::cout << std::fixed << std::setprecision(2) << "DATA GENERATION \n CPU time used: "
              << 1000.0 * (c_end - c_start) / CLOCKS_PER_SEC << " ms\n"
              << "Wall clock time passed: "
              << std::chrono::duration<double, std::milli>(t_end - t_start).count()
              << " ms\n";

    TH1F *p_type = new TH1F("particle_types", "ptypes", 7, 0, 7);
    p_type->SetOption("bar2");
    p_type->SetFillColor(kBlue);
    p_type->SetBarWidth(0.5F);
    p_type->SetBarOffset(0.25F);
    TH1F *invariant_mass = new TH1F("h_imass", "imass", 500, 0, 7);
    TH1F *invariant_mass_d = new TH1F("h_imassd", "imassd", 40, 0.4, 1.4);
    TH1F *invariant_mass_c = new TH1F("h_imassc", "imassc", 40, 0.4, 1.4);
    TH1F *invariant_mass_children = new TH1F("children_imass", "cimass", 30, 0.65, 1.15);
    TH1F *invariant_mass_kp_dis = new TH1F("k_p_d_imass", "kaon_pion_d_imass", 40, 0.4, 1.4);
    TH1F *invariant_mass_kp_con = new TH1F("k_p_c_imass", "kaon_pion_c_imass", 40, 0.4, 1.4);
    TH1F *energy = new TH1F("h_energy", "energy", 400, 0, 5);
    TH1F *impulse = new TH1F("Impulse", "Impulse", 400, 0, 6);
    TH1F *t_impulse = new TH1F("Trasverse_Impulse", "t_Impulse", 400, 0, 4.5);

    {
        int i = 0;
        std::vector<particle_type const *> particles = particle::get_pTypes();
        for (auto const type : particles)
        {
            p_type->GetXaxis()->ChangeLabel(++i, -1, -1, 1, -1, -1, type->get_name());
        }
        p_type->GetXaxis()->ChangeLabel(++i, -1, 0);
        p_type->GetXaxis()->SetNdivisions(7);

        p_type->SetBinContent(1, ppions);
        p_type->SetBinContent(2, npions);
        p_type->SetBinContent(3, pkaons);
        p_type->SetBinContent(4, nkaons);
        p_type->SetBinContent(5, pprotons);
        p_type->SetBinContent(6, nprotons);
        p_type->SetBinContent(7, K);
        p_type->SetEntries(ppions + npions + pkaons + nkaons + pprotons + nprotons + K);
    }

    c_start = std::clock();
    t_start = std::chrono::high_resolution_clock::now();

    for (auto const &e : events)
    {
        for (int i = 0; i != int(e.particles.size()); ++i)
        {
            energy->Fill(e.particles[i].get_pEnergy());
            if (e.particles[i].get_pType_Index() == 6) //K*
            {
                invariant_mass_children->Fill(e.particles[i + 1].get_iMass(e.particles[i + 2]));
                impulse->Fill(e.particles[i].get_pMomentum());
                t_impulse->Fill(e.particles[i].get_pTMomentum());
                i += 2;
            }
            else
            {
                impulse->Fill(e.particles[i].get_pMomentum());
                t_impulse->Fill(e.particles[i].get_pTMomentum());
            }
        }

        for (int i = 0; i != int(e.particles.size()); ++i)
        {
            for (int index = i + 1; index < int(e.particles.size()); ++index)
            {
                invariant_mass->Fill(e.particles[i].get_iMass(e.particles[index]));
                if (e.particles[i].get_pType()->get_charge() * e.particles[index].get_pType()->get_charge() == -1)
                {
                    invariant_mass_d->Fill(e.particles[i].get_iMass(e.particles[index]));

                    if ((e.particles[i].get_pType_Index() == 0 &&      //pion+
                         e.particles[index].get_pType_Index() == 3) || //kaon-

                        (e.particles[i].get_pType_Index() == 1 &&      //pion-
                         e.particles[index].get_pType_Index() == 2) || //kaon+

                        (e.particles[i].get_pType_Index() == 2 &&      //kaon+
                         e.particles[index].get_pType_Index() == 1) || //pion-

                        (e.particles[i].get_pType_Index() == 3 &&    //kaon-
                         e.particles[index].get_pType_Index() == 0)) //pion+
                    {
                        invariant_mass_kp_dis->Fill(e.particles[i].get_iMass(e.particles[index]));
                    }
                }
                else if (e.particles[i].get_pType()->get_charge() * e.particles[index].get_pType()->get_charge() == 1)
                {
                    invariant_mass_c->Fill(e.particles[i].get_iMass(e.particles[index]));
                    if ((e.particles[i].get_pType_Index() == 0 &&      //pion+
                         e.particles[index].get_pType_Index() == 2) || //kaon+

                        (e.particles[i].get_pType_Index() == 1 &&      //pion-
                         e.particles[index].get_pType_Index() == 3) || //kaon-

                        (e.particles[i].get_pType_Index() == 2 &&      //kaon+
                         e.particles[index].get_pType_Index() == 0) || //pion+

                        (e.particles[i].get_pType_Index() == 3 &&    //kaon-
                         e.particles[index].get_pType_Index() == 1)) //pion-
                    {
                        invariant_mass_kp_con->Fill(e.particles[i].get_iMass(e.particles[index]));
                    }
                }
            }
        }
    }
    c_end = std::clock();
    t_end = std::chrono::high_resolution_clock::now();
    std::cout << std::fixed << std::setprecision(2) << "HISTOGRAM FILL \n CPU time used: "
              << 1000.0 * (c_end - c_start) / CLOCKS_PER_SEC << " ms\n"
              << "Wall clock time passed: "
              << std::chrono::duration<double, std::milli>(t_end - t_start).count()
              << " ms\n";
    //Overwrite / Write data to file
    TFile file("file.root", "RECREATE");
    file.WriteTObject(p_type);
    file.WriteTObject(energy);
    file.WriteTObject(invariant_mass);
    file.WriteTObject(invariant_mass_d);
    file.WriteTObject(invariant_mass_c);
    file.WriteTObject(invariant_mass_children);
    file.WriteTObject(invariant_mass_kp_con);
    file.WriteTObject(invariant_mass_kp_dis);
    file.WriteTObject(impulse);
    file.WriteTObject(t_impulse);
    //Free memory
    delete t_impulse;
    delete impulse;
    delete invariant_mass_kp_dis;
    delete invariant_mass_kp_con;
    delete invariant_mass_children;
    delete invariant_mass_c;
    delete invariant_mass_d;
    delete invariant_mass;
    delete energy;
    delete p_type;
    std::cout << std::setprecision(5) << '\n'
              << "ADDED PARTICLE TYPES" << '\n';
    particle::print();
    particle::delete_ptypes();
}