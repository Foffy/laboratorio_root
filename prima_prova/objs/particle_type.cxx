#include "particles.hpp"

//CONSTRUCTORS
/**
 * @brief Construct a new particle type object
 * 
 * @param fName  name of the particle
 * @param mass   mass of the particle
 * @param charge charge of the particle
 */
particle_type::particle_type(std::string const &fName, double const mass, int const charge) : fName_{fName}, mass_{mass}, charge_{charge} {}
resonance_type::resonance_type(std::string const &fName, double const mass, int const charge, double const fWidth) : particle_type::particle_type(fName, mass, charge), fWidth_{fWidth} {}

//PRINT METHODS

/**
 * @brief Print particle type properties
 * 
 */
void particle_type::print() const
{
    std::cout << "Particle Name: " << fName_ << '\n'
              << "Particle Mass: " << mass_ << '\n'
              << "Particle Charge: " << charge_ << '\n';
}
/**
 * @brief Print particle type properties (resonance_type override)
 * 
 */
void resonance_type::print() const
{
    particle_type::print();
    std::cout << "Resonance Width: " << fWidth_ << '\n';
}
