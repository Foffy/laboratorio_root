********************STATISTICAL RESULTS********************

PARTICLE TYPES DISTRIBUTION
___________________________________________________________

Particle Entries:                           20398654
Number of Children:                         398654 ± 625.066223
Particle Entries (K* children excluded):    20000000
Particles Fraction:  

                  -----------------------------------------
                  |    Pion(+):    0.399928 ± 0.000110    |
                  -----------------------------------------
                  |    Pion(-):    0.400116 ± 0.000110    |
                  -----------------------------------------
                  |    Kaon(+):    0.049991 ± 0.000049    |
                  -----------------------------------------
                  |    Kaon(-):    0.049961 ± 0.000049    |
                  -----------------------------------------
                  |    Proton(+):  0.045027 ± 0.000046    |
                  -----------------------------------------
                  |    Proton(-):  0.045010 ± 0.000046    |
                  -----------------------------------------
                  |    K*:         0.009966 ± 0.000022    |
                  -----------------------------------------
___________________________________________________________

EXTRAPOLATED K* Mass & Resonance Width
___________________________________________________________

DISCORDANT CHARGE I.M. - CONCORDANT CHARGE I.M.

K* mass:                     0.887090 ± 0.003441
K* resonance width:          0.051891 ± 0.003540
Gaussian Fit Normalization:  42072.234614 ± 2443.774759
X²obs / nDF Gaussian Fit:    40.659609 / 38
P(X² > X²obs):               0.354065    There is NO EVIDENCE of fit discrepancy
P(z_µ > z_µobs):             0.184057    There is NO EVIDENCE of discrepancy
P(z_σ > z_σobs):             0.593198    There is NO EVIDENCE of discrepancy

KAON-PION DISCORDANT I.M. - CONCORDANT I.M.

K* mass:                     0.893041 ± 0.002027
K* resonance width:          0.053579 ± 0.002039
Gaussian Fit Normalization:  39459.637678 ± 1300.120295
X²obs / nDF Gaussian Fit:    25.335327 / 29
P(X² > X²obs):               0.660736    There is NO EVIDENCE of fit discrepancy
P(z_µ > z_µobs):             0.495735    There is NO EVIDENCE of discrepancy
P(z_σ > z_σobs):             0.079275    There is NO EVIDENCE of discrepancy

CHILDREN INVARIANT MASS

K* mass:                     0.891743 ± 0.000113
K* resonance width:          0.050109 ± 0.000080
Gaussian Fit Normalization:  26447.004093 ± 72.658094
Gaussian Fit X²obs / nDF:    17.588249 / 24
P(X² > X²obs):               0.822514    There is NO EVIDENCE of fit discrepancy
P(z_µ > z_µobs):             0.462979    There is NO EVIDENCE of discrepancy
P(z_σ > z_σobs):             0.170864    There is NO EVIDENCE of discrepancy

WEIGHTED AVERAGE

K* mass:             0.891742 ± 0.000113
K* resonance width:  0.050115 ± 0.000080
P(z_µ > z_µobs):     0.467348    There is NO EVIDENCE of discrepancy
P(z_σ > z_σobs):     0.147446    There is NO EVIDENCE of discrepancy
___________________________________________________________

IMPULSE DISTRIBUTION
___________________________________________________________

Exponential Fit Normalization:  12.611274 ± 0.000322
Exponential Fit Slope:          -0.999749 ± 0.000234
tau:                            1.000251 ± 0.000235
Exponential Fit X²obs / nDF:    380.748885 / 398
P(X² > X²obs):                  0.724678    There is NO EVIDENCE of fit discrepancy
P(z_tau > z_tauobs):            0.285295    There is NO EVIDENCE of discrepancy
___________________________________________________________

PHI - THETA
___________________________________________________________

CORRELATION

Phi-Theta Covariance:             0.000263
Bivariate Uniform Normalization:  49998.941502 ± 11.180221
Bivariate Uniform X²obs / nDF:    422.846152 / 399
P(X² > X²obs):                    0.197280    There is NO EVIDENCE of fit discrepancy 

THETA
Uniform Fit Normalization:  1333332.401676 ± 298.142257
Uniform X²obs/nDF:          13.974854 / 14
P(X² > X²obs):              0.451586    There is NO EVIDENCE of fit discrepancy
PHI
Uniform Fit Normalization:  1333332.427056 ± 298.142269
Uniform X²obs/nDF:          13.594153 / 14
P(X² > X²obs):              0.480363    There is NO EVIDENCE of fit discrepancy
___________________________________________________________

***********************************************************
