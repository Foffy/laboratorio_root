\input{preamble.tex}
\begin{document}

	\maketitle
	\pagenumbering{arabic}
	\tableofcontents
	\newpage
	
	\section{Introduzione}
	\label{sec:Intro}
	Il metodo Monte Carlo consiste nella generazione casuale di campioni di dati che seguono precise distribuzioni e quindi nell'analisi di questi dati per ottenere risultati numerici. In particolare, il programma realizzato, simula eventi di collisione che risultano, secondo proporzioni precise, in un numero noto e finito di tipi di particelle, ciascuna con proprietà cinematiche estratte da popolazioni definite a priori.
	
	
	\section{Struttura del Codice}
	\label{sec:Struct}
	
	Il programma è stato diviso in due eseguibili: uno relativo alla generazione dei dati, e quindi alla scrittura di questi su disco, e uno relativo all'analisi e all'output dei risultati numerici ottenuti, con la valutazione di eventuali discrepanze. Il progetto è stato costruito con l'ausilio di CMake, nell'ottica di ottenere la massima modularità. 
	\subsection{Eseguibile``PARTICLES"}
	\label{sec:particles}
	L'eseguibile ``PARTICLES" si occupa della generazione degli eventi e della scrittura su file dei dati di interesse ed è così definito in ``CMakeLists.txt"
	\lstinputlisting[language=C, firstline=15, lastline=19, caption = eseguibile PARTICLES\text{,} \textit{CMakeLists.txt}]{../CMakeLists.txt}
	\paragraph{Struttura Generale}
	Si è deciso di definire classi e strutture e di dichiararne le funzioni membro nei file header e di definire le funzioni membro nei file sorgente, dai quali CMake costruirà gli object file da linkare, con alcune eccezioni:
	\begin{itemize}
		\item funzioni template, non possono essere definite esternamente, definite in file header
		\item metodi banali, definiti \textit{inline} nei file header
	\end{itemize}
\subparagraph{header files} (directory ``headers/")
\begin{itemize}
	\item event.hpp, definisce la struttura \textit{event}
	\item data\_structures.hpp, definisce la classe template \textit{Vector3D$\langle$FP$\rangle$}
	\item to\_cartesian.hpp, definisce la funzione template di conversione da coordinate sferiche a cartesiane \textit{to\_cartesian}
	\item particle\_types.hpp, definisce le classi \textit{particle\_type} e \textit{resonance\_type}
	\item particles.hpp, definisce la classe \textit{particle}
\end{itemize}
\subparagraph{source files} (directory ``objs/")
\begin{itemize}
	\item generate\_data.cxx, definisce la funzione \textit{main()}
	\item particle\_types.cxx, definisce le funzioni dichiarate in particle\_types.hpp
	\item particles.cxx, definisce le funzioni dichiarate in particles.hpp
\end{itemize}
Questa struttura modulare consente di modificare facilmente definizioni agendo sui singoli moduli, senza la necessità di dover ricompilare tutto il progetto.
\paragraph{Gestione dell'Informazione}

	Le informazioni condivise dai tipi di particelle, per evitare rindondanza,  sono contenute in oggetti allocati nello heap di classe \textit{particle\_type} o \textit{resonance\_type}:\\
	\begin{wrapfigure}[8]{rH}{0.4\textwidth}
		\centering
		\begin{tikzpicture}[node distance = 2cm]
			\node (ptype) [class] {particle\_type};
			\node (rtype) [class, below of=ptype] {resonance\_type};
			\draw[arrow] (rtype) -- node[anchor=west] {is a} (ptype);
		\end{tikzpicture}
		\caption{relazione di ereditarietà tra classi}
	\end{wrapfigure}
	\begin{itemize}
		\item nome - \textit{std::string const particle\_type::fName\_}
		\item massa - \textit{double const particle\_type::mass\_}
		\item carica - \textit{int const particle\_type::charge\_}
		\item risonanza - \textit{double const resonance\_type::fWidth\_} \\\\\\
	\end{itemize}
	Questi oggetti sono costruiti dal metodo statico \textit{particle::add\_particle\_type}, uno per ogni tipo di particella aggiunto, e puntati dagli elementi di un array: \textit{static particle::std::vector$\langle$particle\_type const *$\rangle$ particles\_}.
	Le informazioni uniche di ogni particella sono conservate in oggetti di classe \textit{particle}:
	\begin{itemize}
		\item informazione cinematica - \textit{Vector3D$\langle$float$\rangle$ particle::momentum\_}
		\item indirizzo del tipo di particella nell'array di \textit{particle\_type *} - \textit{int particle::index\_}
	\end{itemize}
	Tramite i metodi \textit{get} è possibile, per ogni oggetto \textit{particle}, ottenere tutte le informazioni della particella.
	
	\subsection{Eseguibile ``HISTOGRAMS"}
	\label{sec:histograms}
	L'eseguibile ``HISTOGRAMS" è così definito in ``CMakeLists.txt":
	\lstinputlisting[language=C, firstline=20, lastline=22, caption = eseguibile HISTOGRAMS\text{,} \textit{CMakeLists.txt}]{../CMakeLists.txt}
	Questa parte del programma è composta da un unico file sorgente ``main.cpp" che, utilizzando le classi e le funzioni fornite da \textit{ROOT}, estrae i dati, esegue fit, genera le immagini e valuta le discrepanze.
	
	
	
	\section{Generazione Dati}
	\label{sec:Gen}
	In una esecuzione di ``PARTICLES" vengono generati \(N\) oggetti \textit{event} inseriti in un array. Ogni \textit{event} contiene un array di \textit{particle} di dimensione maggiore o uguale a \(100\), ciò è dovuto al decadimento delle risonanze, i cui prodotti (coppia \((\pi, \kappa)\) di carica discorde) occuperanno i due indirizzi successivi alle stesse. \(N\) è un intero positivo costante, noto al compile time, nella generazione presa in considerazione \(N = 2\cdot 10^{5}\). Le estrazioni casuali sono fatte grazie ad un PRNG  (Mersenne Twister, \textit{std::mt19937}\cite{cppreference_mt}) che viene inizializzato ad ogni esecuzione, con un seed ottenuto tramite \textit{std::random\_device}, che ritorna un valore casuale, non deterministico, distribuito uniformemente\cite{cppreference_rdev}.
			\lstinputlisting[language=C++, firstline=44, lastline=44, caption = inizializzazione PRNG\text{,} \textit{generate\_data.cxx}]{../objs/generate_data.cxx}
	Tutti i dati, esclusi i tipi di particelle (\autoref{sec:ptypes}) e le coordinate \((\theta, \phi)\) dell'impulso (\autoref{sec:thetaphi}), sono ottenuti con i metodi \textit{get}, e inseriti negli istogrammi, in un secondo ciclo che scorre tutto il vettore di \textit{event} con un range for. Alla fine dell'esecuzione otterremo due file \textit{.root}:
	\begin{itemize}
		\item file.root
		\begin{itemize}
			\item \textit{TH1F particle\_types}, istogramma tipi di particelle
			\item \textit{TH1F  h\_energy}, istogramma energia
			\item \textit{TH1F h\_imass}, istogramma massa invariante tra tutte le particelle
			\item \textit{TH1F h\_imassd}, istogramma massa invariante tra tutte le particelle di carica discorde
			\item \textit{TH1F h\_imassc}, istogramma massa invariante tra tutte le particelle di carica concorde
			\item \textit{TH1F children\_imass}, istogramma massa invariante tra particelle figlie di \(K^{*}\)
			\item \textit{TH1F k\_p\_c\_imass}, istogramma massa invariante tra particelle \(\kappa, \pi\) di carica concorde
			\item \textit{TH1F k\_p\_d\_imass}, istogramma massa invariante tra particelle \(\kappa, \pi\) di carica discorde
			\item \textit{TH1F Impulse}, istogramma impulso
			\item \textit{TH1F Trasverse\_impulse}, istogramma impulso trasverso
		\end{itemize}
		\item ANGLES.root
		\begin{itemize}
			\item \textit{TTree angles}, root tree contenente tutte le coppie \((\theta, \phi)\) generate
		\end{itemize}
	\end{itemize}
	\subsection{Tipi di Particelle}
	\label{sec:ptypes}
	\begin{table}[H]
		\begin{center}
			\begin{tabular}{|l|c|c|c|c|}
				\hline
				\textbf{Tipo di Particella} & Simbolo & \(\mathrm{m_{0}}\) (\(\mathrm{GeV}\times \mathrm{c}^{-2}\)) & \(\mathrm{q}\) \((q_{e})\) & \(\Gamma\) (\(\mathrm{GeV}\times \mathrm{c}^{-2}\))\\
				\hline
				Pione(+) & \(\pi^{+}\) & 0.13957 & +1 & / \\
				\hline
				Pione(-) & \(\pi^{-}\) & 0.13957 & -1 & / \\
				\hline
				Kaone(+) & \(\kappa^{+}\) & 0.49367 & +1 & / \\
				\hline
				Kaone(-) & \(\kappa^{-}\) & 0.49367 & -1 & / \\
				\hline
				Protone(+) & \(p^{+}\) & 0.93827 & +1 & / \\
				\hline
				Protone(-) & \(p^{-}\) & 0.93827 & -1 & / \\
				\hline
				Risonanza & \(K^{*}\) & 0.89166 & 0 & 0.05 \\
				\hline
				
			\end{tabular}
		\end{center}
		\caption{Proprietà tipi di particelle}
		\label{fig:ptypes}
	\end{table}
	I tipi di particelle vengono aggiunti tramite il metodo \textit{particle::add\_particle\_type}
		\lstinputlisting[language=C++, firstline=18, lastline=24, caption = aggiunta tipi di particelle\text{,} \textit{generate\_data.cxx}]{../objs/generate_data.cxx}
	Estraendo valori distribuiti secondo una uniforme \(\Phi_{x}(x) = 1,\  x \in [0,1]\) (\autoref{lst:uniform}), possiamo ottenere particelle i cui tipi sono distribuiti nelle proporzioni desiderate (\autoref{fig:abundance})
	\lstinputlisting[language=C++, linerange={50-50, 60-60}, caption = estrazione \textit{x}\cite{cppreference_uniform}\text{,}
	 \textit{generate\_data.cxx}, label=lst:uniform]{../objs/generate_data.cxx}
	\begin{table}[H]
		\begin{center}
			\begin{tabular}{|l|c|}
				\hline
				\textbf{Tipo di Particella} & Abbondanza (\%)\\
				\hline
				\(\pi^{+}\) & 40 \\
				\hline
				\(\pi^{-}\) & 40 \\
				\hline
				\(\kappa^{+}\) & 5 \\
				\hline
				\(\kappa^{-}\) & 5 \\
				\hline
				\(p^{+}\) & 4.5 \\
				\hline
				\(p^{-}\) & 4.5\\
				\hline
				\(K^{*}\) & 1 \\
				\hline
				
			\end{tabular}
		\end{center}
	\caption{Abbondanza a priori dei tipi di particelle}
	\label{fig:abundance}
	\end{table}
	Viene tenuto traccia del numero di ogni tipo di particella tramite contatori interi, che vengono poi utilizzati per riempire i bin di un istogramma, così da non doverli poi riottenere dall'array di \textit{event}.
	\lstinputlisting[language=C++, firstline=167, lastline=174, caption = riempimento istogramma p\_type\text{,} \textit{generate\_data.cxx}]{../objs/generate_data.cxx}
	\subsection{Impulso}
	L'impulso delle particelle viene generato in coordinate sferiche \(\overrightarrow{P}_{p} = \left(\rho, \theta, \phi \right)\), per poi essere passato al costruttore di \textit{particle} come \textit{Vector3D$\langle$float$\rangle$} in coordinate cartesiane  \(\overrightarrow{P}_{c} = (P_{x}, P_{y}, P_{z})\).
	\lstinputlisting[language=C++, firstline=66, lastline=66, caption = costruzione di un oggetto \textit{particle}\text{,} \textit{generate\_data.cxx}]{../objs/generate_data.cxx}
	\subsubsection{Modulo}
	\label{sec:rhodistr}
	Il modulo \(\rho\) dell'impulso di ogni particella viene estratto da una distribuzione esponenziale:
	\begin{equation}
	\Phi_{\rho}(\rho) = \frac{1}{\tau}e^{-\frac{\rho}{\tau}}, \ \rho \in [0, +\infty[, \ \tau = 1
	\label{eq:rhodistr}
    \end{equation}
	\lstinputlisting[language=C++, linerange= {53-53, 61-61}, caption = estrazione di \(\rho\)\cite{cppreference_expo}\text{,} \textit{generate\_data.cxx}]{../objs/generate_data.cxx}
	\subsubsection{\(\theta, \phi\)}
	\label{sec:thetaphi}
	Le coordinate angolari dell'impulso vengono estratte da distribuzioni uniformi in modo che non siano correlate.
	\begin{equation}
		\Phi_{\theta}(\theta) = \frac{1}{\pi}, \ \theta \in [0, \pi]
		\label{eq:thetadistr}
	\end{equation}
	\begin{equation}
	\Phi_{\phi}(\phi) = \frac{1}{2\pi}, \ \phi \in [0, 2\pi]
	\label{eq:phidistr}
    \end{equation}
    \lstinputlisting[language=C++, linerange = {51-52,57-58}, caption = estrazione di \(\theta\) e \(\phi\)\cite{cppreference_uniform}\text{,} \textit{generate\_data.cxx}, label=lst:genthetaphi]{../objs/generate_data.cxx}
    Ogni valore della coppia \((\theta, \phi)\) viene scritto direttamente su disco alla fine del ciclo di generazione grazie ad un \textit{TTree}, conservando l'informazione di correlazione per poterla poi valutare.
	\section{Analisi Dati}
	La lettura dati, l'output grafico degli istogrammi e la valutazione della discrepanza, è svolta dall'eseguibile \textit{HISTOGRAMS}, l'output testuale è riportato in \autoref{A:aout} (\autoref{lst:out}). Prima dell'analisi deve essere eseguita la generazione dati tramite \textit{PARTICLES}, in questo caso si è preso in esame un campione di \(N = 2\cdot10^{5}\) eventi.
	
	\label{sec:asys}
	\subsection{Tipi di Particelle}
	Il conteggio dei tipi di particelle è descrivibile con una distribuzione binomiale:
	\[P(k) = \left(\! \! \begin{array}{c}n\\ k \end{array}\! \! \right)p^{k}(1-p)^{n-k}\] 
	Possiamo quindi stimare l'errore sulla frazione di ogni tipo di particella come:
	\begin{equation}
	\tilde{\sigma}_{p} = \frac{\sqrt{np^{*}(1-p^{*})}}{n}
	\label{eq:sigmap}
\end{equation}
Dove \(p^{*}\) è il valore della frazione di particelle osservato, mentre l'errore sui conteggi:
\begin{equation}
	\tilde{\sigma}_{np} = \sqrt{np^{*}(1 - p^{*})}
\end{equation}



	\subsection{Impulso}
	\subsubsection{Modulo}
	Ci si aspetta che il modulo dell'impulso sia distribuito in modo coerente con la distribuzione esponenziale \(\Phi_{\rho}(\rho)\) discussa \eqref{eq:phidistr}. Si è proceduto ad un fit (\autoref{fig:four}), da cui si è ottenuta la stima del parametro \(\tilde{\tau} = -\frac{1}{\mathrm{slope}}\) e \(\tilde{\sigma}_{\tau} = \frac{1}{\mathrm{slope}^{2}}\sigma_{\mathrm{slope}}\). 
	\begin{theorem}
		\(\tilde{\tau}\), per campioni grandi, risulterà distribuita come una variabile gaussiana
		\begin{equation}
		 G(\tilde{\tau}; \tau; \sigma_{\tau}) = \frac{1}{\sqrt{2\pi}\sigma_{\tau}}\mathrm{exp}\left[{-\frac{(\tilde{\tau} - \tau)^{2}}{2\sigma_{\tau}^{2}}}\right]
		\end{equation}
	\end{theorem}
\begin{proof}
	\( \tilde{\tau} = \frac{1}{n}\sum_{i = 1}^{n} x_{i} \), \(\tilde{\tau}\) può essere dunque scritto come combinazione lineare di grandezze casuali con coefficienti \(\lambda_{1}, ... , \lambda_{n} = \frac{1}{n}\). Queste grandezze sono estratte da una popolazione distribuita esponenzialmente con media \(\tau\) e deviazione standard \(\sigma_{x} = \tau\). Allora per il \textit{teorema del limite centrale}\cite{fornasiniclt} \(\tilde{\tau}\) sarà distribuito secondo una gaussiana di media \(\mu = \sum_{n}^{i=1}\frac{1}{n}\tau =\tau\) e di deviazione standard \(\sigma_{\tau} = \sqrt{\sum_{n}^{i=1}\frac{1}{n^{2}}\tau^{2}} = \frac{\tau}{\sqrt{n}}\)
	\label{th:thclt}
\end{proof}
Poiché \(\tilde{\tau}\) è con buona approssimazione una grandezza gaussiana, possiamo eseguire un test di compatibilità col valore reale \(\tau\). È stata quindi calcolata la variabile normale standard \(z_{o} = \frac{\tilde{\tau} - \tau}{\tilde{\sigma}_{\tau}}\) e trovata \(P(z > z_{o})\), inoltre si è estratto il valore del \(\tilde{\chi}^{2}_{o}\) del fit e quindi \(P(\tilde{\chi}^{2}_{\nu} > \tilde{\chi}^{2}_{o})\). 
	\subsubsection{\(\theta, \phi\)}
	\label{sec:anthetaphi}
	
 Per verificare che \(\theta\) e \(\phi\) seguano le distribuzioni precedentemente definite (\autoref{sec:thetaphi}), è stato eseguito un fit (\autoref{fig:four}) ed un test del \(\chi^{2}\).
  Per escludere che vi sia una correlazione tra le due grandezze, è stata ottenuta la covarianza \(\sigma_{\theta \phi} \simeq 0\) e verificata l'equiprobabilità di ogni coppia di angoli con un fit di una uniforme bivariata (\autoref{fig:biunifit}) e quindi un test del \(\chi^{2}\).
  \subsection{Segnale K*}
	Il segnale della risonanza, negli istogrammi di massa invariante, deve essere estratto dal fondo, si sfrutta quindi la conservazione della carica. I prodotti del decadimento di \(K^{*}\) (coppia \(\pi, \kappa\)) devono avere complessivamente carica nulla, quindi opposta; dunque gli istogrammi delle masse invarianti tra particelle di carica concorde contengono solo fondo, mentre quelli di carica discorde contengono fondo e segnale, sottraendoli e eseguendo dei fit gaussiani sugli istogrammi risultanti (\autoref{fig:imasses}), otteniamo i dati d'interesse, ovvero massa a riposo (\(m^{*}_{0} = \tilde{\mu}\)) e risonanza (\(\Gamma^{*} = \tilde{\sigma}\)) di \(K^{*}\). Sono stati eseguiti test del \(\chi^{2}\) e di compatibilità.
	\subsection{Risultati}
	\subsubsection{Grafici}
	\begin{figure}[H]
		\includegraphics[width=\linewidth]{OUTPUT/four.png}
		\caption{Root Canvas divisa in 4: istogramma dei tipi di particelle e distribuzioni delle componenti dell'impulso}
		\label{fig:four}
	\end{figure}
	\begin{figure}[H]
		\centering
		\includegraphics[width=1\linewidth]{OUTPUT/phitheta_corr.png}
		\caption{Distribuzione di \((\theta, \phi)\) e fit uniforme}
		\label{fig:biunifit}
	\end{figure}
	\begin{figure}[H]
		\centering
		\includegraphics[width=1\linewidth]{OUTPUT/imasses.png}
		\caption{Root Canvas divisa in 3: istogrammi di massa invariante, segnale della risonanza}
		\label{fig:imasses}	
	\end{figure}
	\subsubsection{Dati Ottenuti}
	\begin{table}[H]
		\begin{center}
			\begin{tabular}{|c|c|c|c|}
				\hline
				\textbf{Tipo di Particella} &\(p^{*}\pm \tilde{\sigma}_{p}(\%)\)  & \(p(\%)\) & \(\sigma_{p}(\%)\)\\
				\hline
				\(\pi^{+}\) & \(39.99 \pm 0.01\) & 40 & 0.01\\
				\hline
				\(\pi^{-}\) & \(40.01 \pm 0.01\) & 40 & 0.01\\
				\hline
				\(\kappa^{+}\) & \(4.999 \pm 0.005\) & 5 & 0.005  \\
				\hline
				\(\kappa^{-}\) & \(4.996 \pm 0.005\) & 5 & 0.005 \\
				\hline
				\(p^{+}\) & \(4.503 \pm 0.005\) & 4.5 & 0.005\\
				\hline
				\(p^{-}\) & \(4.501 \pm 0.005\) & 4.5 & 0.005\\
				\hline
				\(K^{*}\) & \(0.997 \pm 0.002\) & 1 & 0.002\\
				\hline
				
			\end{tabular}
		\end{center}
		\caption{Abbodanza di particelle misurata e attesa. I prodotti di decadimento di \(K^{*}\) risultano essere \((3.987 \pm 0.006)\cdot10^{5}\) con un valore atteso di \(4.000\cdot10^{5}\) e \(E[\sigma] = 6\cdot10^{2} \)}
		\label{fig:abresults}
	\end{table}
\begin{table}[H]
	\begin{center}
		\begin{tabular}{|c|c|c|c|c|c|}
			\hline
			\textbf{Distribuzione} & Parametro Fit & \(\chi^{2}_{o}\) & \(\nu\) & \(\tilde{\chi}^{2}_{o}\)& \(P(\chi^{2}_{\nu} > \chi^{2}_{o})\) \\
			\hline
			\(\rho\) & \(\mathrm{slope} = -(0.9997 \pm 0.0002)\mathrm{GeV^{-1} \times c}\) & 381 & 398 & 0.957 & 0.725  \\
			\hline
			\(\theta\) & \(p_{0} = (1.3333 \pm 0.0003)\cdot10^{6}\) & 14.0 & 14 & 1.00 & 0.452\\
			\hline
			\(\phi\) & \(p_{0} = (6.667 \pm 0.002)\cdot10^{5}\) & 13.6 & 14 & 0.971 & 0.480\\
			\hline
			\((\theta, \phi)\) & \(p_{0} = (8.889 \pm 0.002)\cdot10^{4}\) & 423 & 399 & 1.06 & 0.197\\
			\hline

			
		\end{tabular}
	\end{center}
	\caption{Fit sulle distribuzioni delle componenti dell'Impulso e test del \(\chi^{2}\)}
	\label{fig:fitresults}
\end{table}

\begin{table}[H]
	\begin{center}
		\begin{tabular}{|l|c|c|c|c|c|c|}
			\hline
			\textbf{Distribuzione} &\(i\) &\(\tilde{\mu}_{i} \ (\mathrm{GeV \times c^{-2}})\) & \(\tilde{\sigma}_{i} \ (\mathrm{GeV \times c^{-2}})\) & Ampiezza & \(\tilde{\chi}^{2}_{o}\)& \(P(\chi^{2}_{\nu} > \chi^{2}_{o})\) \\
			\hline
			Segnale Reale & 1 & \(0.8917 \pm 0.0001\) & \(0.05011 \pm 0.00008\) & \((2.645 \pm 0.007)\cdot10^{4}\) & 0.733 & 0.823  \\
			\hline
			Carica Discorde - & 2 & \(0.887 \pm 0.003\) & \(0.052 \pm 0.004\) & \((4.2 \pm 0.2)\cdot10^{4}\) & 1.07 & 0.354\\
			Carica Concorde& & & & & & \\
			\hline
			Carica Discorde - & 3 & \(0.893 \pm 0.002\) & \(0.054 \pm 0.002\) & \((3.9 \pm 0.1)\cdot10^{4}\) & 0.874 & 0.661\\
			Carica Concorde \((\pi,\kappa)\)& & & & & & \\
			\hline
			
			
		\end{tabular}
	\end{center}
	\caption{Fit gaussiano sulle distribuzioni di massa invariante e test del \(\chi^{2}\)}
	\label{fig:imfitresults}
\end{table}
\begin{table}[H]
	\begin{center}
		\begin{tabular}{|l|c|c|c|c|}
			\hline
			\textbf{Parametro} & \(\tilde{x} \pm \tilde{\sigma}_{x}\) & \(x\) & \(|z_{o}|\) &\(P(z > |z_{o}|)\) \\
			\hline
			\(\tilde{\tau}\) & \((1.0003 \pm 0.0002)\mathrm{GeV \times c^{-1}}\) & \(1\mathrm{GeV \times c^{-1}}\)& 1.07 & 0.285   \\
			\hline
			\(\tilde{\mu}_{1}\) & \((0.8917 \pm 0.0001)\mathrm{GeV \times c^{-2}}\) & \(0.89166\mathrm{GeV \times c^{-2}}\) & 0.735 &  0.463 \\
			\hline
			\(\tilde{\sigma}_{1}\) & \((0.05011 \pm 0.00008)\mathrm{GeV \times c^{-2}}\) & \(0.05\mathrm{GeV \times c^{-2}}\) & 1.36 &  0.171 \\
			\hline
			\(\tilde{\mu}_{2}\) & \((0.887 \pm 0.003)\mathrm{GeV \times c^{-2}}\) & \(0.89166\mathrm{GeV \times c^{-2}}\) & 1.33 &  0.184 \\
			\hline
			\(\tilde{\sigma}_{2}\) & \((0.052 \pm 0.004)\mathrm{GeV \times c^{-2}}\) & \(0.05\mathrm{GeV \times c^{-2}}\) & 0.534 &  0.593 \\
			\hline
			\(\tilde{\mu}_{3}\) & \((0.893 \pm 0.002)\mathrm{GeV \times c^{-2}}\) & \(0.89166\mathrm{GeV \times c^{-2}}\) & 0.681 &  0.496 \\
			\hline
			\(\tilde{\sigma}_{3}\) & \((0.054 \pm 0.002)\mathrm{GeV \times c^{-2}}\) & \(0.05\mathrm{GeV \times c^{-2}}\) & 1.76 &  0.0793 \\
			\hline
			
			
		\end{tabular}
	\end{center}
	\caption{Parametri ottenuti dai fit e test di compatibilità con i valori attesi}
	\label{fig:valuestest}
\end{table}
	
	\bibliographystyle{plain}
	\bibliography{refs}
	
	\appendix	
	\section{Output Programma}
	\label{A:aout}
	\begin{lstlisting}[caption={Output \textit{PARTICLES}}]
	DATA GENERATION 
	CPU time used: 8986.35 ms
	Wall clock time passed: 9354.92 ms
	HISTOGRAM FILL 
	CPU time used: 94528.42 ms
	Wall clock time passed: 94563.22 ms
	
	ADDED PARTICLE TYPES
	Particle Name: pion+
	Particle Mass: 0.13957
	Particle Charge: 1
	
	Particle Name: pion-
	Particle Mass: 0.13957
	Particle Charge: -1
	
	Particle Name: kaon+
	Particle Mass: 0.49367
	Particle Charge: 1
	
	Particle Name: kaon-
	Particle Mass: 0.49367
	Particle Charge: -1
	
	Particle Name: proton+
	Particle Mass: 0.93827
	Particle Charge: 1
	
	Particle Name: proton-
	Particle Mass: 0.93827
	Particle Charge: -1
	
	Particle Name: K*
	Particle Mass: 0.89166
	Particle Charge: 0
	Resonance Width: 0.05000	
	\end{lstlisting}
	\lstinputlisting[language = bash, label = {lst:out}, caption = Output \textit{HISTOGRAMS}]{OUTPUT/results.txt}
	\section{Compilazione o Build}
	Il programma è stato costruito e testato con l'ausilio di CMake su una distribuzione Linux (Manjaro KDE x64), tuttavia può essere anche compilato manualmente su linea di comando:
	\begin{lstlisting}[language = bash, caption = {Compilazione eseguibile \textit{PARTICLES}}]
		$ g++ /Path/To/objs/generate_data.cxx  /Path/To/objs/particles.cxx /Path/To/objs/particle_type.cxx -I /Path/To/headers/ -std=c++17 `root-config --cflags --libs` -O3 -o PARTICLES
	\end{lstlisting}
	\begin{lstlisting}[language = bash, caption = {Compilazione eseguibile \textit{HISTOGRAMS}}]
		$ g++ /Path/To/histograms/main.cpp -std=c++17 `root-config --cflags --libs` -O3 -o HISTOGRAMS
	\end{lstlisting}
	Per costruire il progetto con CMake:
	\begin{lstlisting}[language = bash, caption = {Costruzione intera con CMake (generatore GNU Make)}]
		$ mkdir /Path/To/build
		$ cmake /Path/To/Project_Folder /Path/To/build
		$ cd /Path/To/build
		$ make -j4
	\end{lstlisting}
	\section{Listato Codice}
	\subsection{CMakeLists.txt}
	\lstinputlisting[language = bash]{../CMakeLists.txt}
	\subsection{Headers}
	\subsubsection{coord\_converter.hpp}
	\lstinputlisting[language = C++]{../headers/coord_converter.hpp}
	\subsubsection{event.hpp}
	\lstinputlisting[language = C++]{../headers/event.hpp}
	\subsubsection{data\_structures.hpp}
	\lstinputlisting[language = C++]{../headers/data_structures.hpp}
	\subsubsection{particle\_type.hpp}
	\lstinputlisting[language = C++]{../headers/particle_type.hpp}
	\subsubsection{particles.hpp}
	\lstinputlisting[language = C++]{../headers/particles.hpp}
	\subsection{Source Files}
	\subsubsection{particle\_type.cxx}
	\lstinputlisting[language = C++]{../objs/particle_type.cxx}
	\subsubsection{particles.cxx}
	\lstinputlisting[language = C++]{../objs/particles.cxx}
	\subsubsection{generate\_data.cxx}
	\lstinputlisting[language = C++]{../objs/generate_data.cxx}
	\subsection{HISTOGRAMS}
	\subsubsection{main.cpp}
	\lstinputlisting[language = C++]{../histograms/main.cpp}
	
	
	
	
	
\end{document}