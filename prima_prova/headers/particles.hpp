#ifndef PARTICLES_HPP
#define PARTICLES_HPP
#include "data_structures.hpp"
#include "particle_type.hpp"
#include <cmath>
#include <string>
#include <iostream>
#include <vector>
/**
 * @brief Class representing a single particle 
 * 
 */
class particle
{
public:
    particle(std::string const &fName, Vector3D<float> const &momentum);
    particle(int const index, Vector3D<float> const &momentum);
    particle() = default;
    static void add_particle_type(std::string const &fName, double const mass, int const charge, double const resonance = -1);
    static void delete_ptypes();
    particle_type const *get_pType() const;
    double get_iMass(particle const &p2) const;
    double get_pEnergy() const;
    inline double get_pMass() const;
    inline double get_pMomentum() const;
    inline double get_pTMomentum() const;
    inline int get_pType_Index() const;
    int Decay2body(particle &dau1, particle &dau2) const;
    void print_pProperties() const;
    void set_pType(std::string const &fName);
    void set_pType(int const index);
    void set_Momentum(Vector3D<float> const &m);
    void set_Momentum(float const rho, float const phi, float const theta);
    inline static void print()
    {
        for (auto const &i : particles_)
        {
            i->print();
            std::cout << '\n';
        }
    }
    inline static std::vector<particle_type const *> const &get_pTypes()
    {
        return particles_;
    }

private:
    Vector3D<float> momentum_;
    int index_;
    static std::vector<particle_type const *> particles_;
    static int fNParticle_Types_;
    static int Max_PTypes_;

    void Boost(double bx, double by, double bz);
    static int Find_pType(std::string const &fName);
};

//GET METHODS

/**
 * @brief Get particle type
 * 
 * @return particle_type const* <- pointer to particle type
 */
inline particle_type const *particle::get_pType() const
{
    return particles_[index_];
}
/**
 * @brief Get particle Mass
 * 
 * @return double <- Mass
 */
inline double particle::get_pMass() const
{
    return particles_[index_]->get_mass();
}

/**
 * @brief Get particle momentum
 * 
 * @return double <- momentum
 */
inline double particle::get_pMomentum() const
{
    return momentum_.get_Norm();
}
/**
 * @brief Get particle trasverse momentum
 * 
 * @return double <- transverse momentum
 */
inline double particle::get_pTMomentum() const
{
    return sqrt(momentum_.x() * momentum_.x() + momentum_.y() * momentum_.y());
}
/**
 * @brief Get particle type index
 * 
 * @return int <- Index in static member particles_
 */
inline int particle::get_pType_Index() const
{
    return index_;
}

#endif