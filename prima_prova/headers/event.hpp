#ifndef GENERATE_EVENT_HPP
#define GENERATE_EVENT_HPP
#include "particles.hpp"
#include <vector>
/**
 * @brief Data structure representing a single generation event
 * 
 */
struct event
{
    std::vector<particle> particles;
};
#endif
