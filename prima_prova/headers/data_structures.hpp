#ifndef DATA_STRUCTURES_HPP
#define DATA_STRUCTURES_HPP
#include <type_traits>
#include <iostream>
#include <cmath>

template <typename FP>
/**
 * @brief Class representing a 3D Vector
 * 
 */
class Vector3D
{
public:
    Vector3D<FP>(FP x = FP{}, FP y = FP{}, FP z = FP{}) : x_{x}, y_{y}, z_{z} {}
    inline Vector3D<FP> operator()(FP x, FP y, FP z)
    {
        x_ = static_cast<FP>(x);
        y_ = static_cast<FP>(y);
        z_ = static_cast<FP>(z);
        return *this;
    }
    void print() const
    {
        std::cout << "( " << x_ << " , " << y_ << " , " << z_ << " )" << '\n';
    }
    inline Vector3D<FP> operator+(Vector3D<FP> const &v) const
    {
        return Vector3D<FP>(x_ + v.x_, y_ + v.y_, z_ + v.z_);
    }
    inline Vector3D<FP> operator-(Vector3D<FP> const &v) const
    {
        return Vector3D<FP>(x_ - v.x_, y_ - v.y_, z_ - v.z_);
    }
    inline Vector3D<FP> operator*(FP const &c) const // dot product between scalar and vector
    {
        return Vector3D<FP>(c * x_, c * y_, c * z_);
    }
    inline FP operator*(Vector3D<FP> const &v) const // dot product between vectors
    {
        return x_ * v.x_ + y_ * v.y_ + z_ * v.z_;
    }
    inline FP get_Norm() const
    {
        return sqrt(x_ * x_ +
                    y_ * y_ +
                    z_ * z_);
    }
    inline FP x() const
    {
        return x_;
    }
    inline FP y() const
    {
        return y_;
    }
    inline FP z() const
    {
        return z_;
    }

private:
    static_assert(std::is_floating_point_v<FP>);
    FP x_;
    FP y_;
    FP z_;
};
#endif