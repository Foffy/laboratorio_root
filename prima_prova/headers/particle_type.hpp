#ifndef PARTICLE_TYPE_HPP
#define PARTICLE_TYPE_HPP
#include <string>
class particle_type
{
public:
    particle_type(std::string const &fName, double const mass, int const charge);
    particle_type() = default;
    virtual ~particle_type() = default;
    inline std::string const &get_name() const;
    inline double get_mass() const;
    inline virtual double get_fWidth() const
    {
        return 0;
    }
    inline int get_charge() const;
    virtual void print() const;

private:
    std::string const fName_;
    double const mass_;
    int const charge_;
};
class resonance_type : public particle_type
{
public:
    resonance_type(std::string const &fName, double const mass, int const charge, double const fWidth);
    inline double get_fWidth() const;
    void print() const override;

private:
    double const fWidth_;
};

/////////////////////////
//   inline methods    //
//     definitions     //
/////////////////////////

/**
 * @brief Get resonance width
 * 
 * @return double <- resonance width
 */
inline double resonance_type::get_fWidth() const
{
    return fWidth_;
}
/**
 * @brief Get particle type name
 * 
 * @return std::string const& <- string representing the name
 */
inline std::string const &particle_type::get_name() const
{
    return fName_;
}
/**
 * @brief Get the mass of the particle
 * 
 * @return double <- mass of the particle
 */
inline double particle_type::get_mass() const
{
    return mass_;
}
/**
 * @brief Get the charge of the particle
 * 
 * @return int <- charge of the particle
 */
inline int particle_type::get_charge() const
{
    return charge_;
}
#endif