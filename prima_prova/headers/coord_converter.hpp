#ifndef COORD_CONVERTER_HPP
#define COORD_CONVERTER_HPP
#include "data_structures.hpp"
#include <type_traits>

template <typename FP>
/**
 * @brief This function converts spherical coordinates into cartesian coordinates
 * 
 * @param rho vector norm
 * @param theta inclination
 * @param phi azimuth
 * @return Vector3D<FP> - cartesian coordinates vector
 */
inline Vector3D<FP> to_cartesian(FP const rho, FP const theta, FP const phi)
{
    static_assert(std::is_floating_point_v<FP>);
    if (phi > 2 * M_PI || phi < 0 || theta > M_PI || theta < 0)
    {
        throw std::logic_error("Invalid Angle Value");
    }
    FP x = rho * std::sin(theta) * std::cos(phi);
    FP y = rho * std::sin(theta) * std::sin(phi);
    FP z = rho * std::cos(theta);
    return Vector3D<FP>{x, y, z};
}
#endif